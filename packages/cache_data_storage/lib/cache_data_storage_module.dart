import 'dart:async';
import 'dart:math';

import 'package:hive_flutter/adapters.dart';
import 'package:injectable/injectable.dart';
import 'package:modules_basis/module.dart' as module;
import 'package:modules_basis/modules_basis.dart' show ContainerDI;

import 'domain/cache_data_storage.dart';
import 'infrastructure/hive_cache_storage.dart';

@InjectableInit.microPackage()
@lazySingleton
class CacheDataStorageModule
    extends module.Module<CacheDataStorageModuleResult> {
  final ContainerDI containerDI;

  CacheDataStorageModule(
    this.containerDI,
  );

  @override
  Future<CacheDataStorageModuleResult> execute() async {
    await Hive.initFlutter();

    // TODO Ilya: bad solution! (disk multiple folders) Maybe try to open new if isBoxOpen == true
    // Postfix fixes error with multiple windows on the Windows
    final randomPostfix = Random.secure().nextInt(1000);
    // TODO Ilya: try to use openLazyBox for optimization (registerLazySingleton)
    // TODO Ilya: try to use Encrypted box https://docs.hivedb.dev/#/advanced/encrypted_box
    // $randomPostfix
    const boxName = 'CacheDataStorageModuleBox';
    final box = await Hive.openBox(
      boxName,
      // Hive.isBoxOpen(boxName) ? boxName : '$boxName$randomPostfix',
    );

    containerDI.registerSingleton<CacheDataStorage>(
      HiveCacheStorage(box),
    );

    // containerDI.registerFactoryParam<CacheDataStorage, String, void>(
    //   (name, _) => HiveCacheStorage(Hive.box(name)),
    // );

    return CacheDataStorageModuleResult();
  }

  @disposeMethod
  void dispose() {
    containerDI.unregister<CacheDataStorage>();
  }
}

class CacheDataStorageModuleResult extends module.ModuleResult {}
