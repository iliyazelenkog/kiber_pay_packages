import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:syncfusion_flutter_core/theme.dart';
import 'package:syncfusion_flutter_datagrid/datagrid.dart';

class TableWidgetPresenter {
  final DataGridController tableController;
  final bool highlightFooter;
  final void Function(String copiedText)? onCopy;

  TableWidgetPresenter({
    required this.tableController,
    required this.highlightFooter,
    this.onCopy,
  });

  late final customSelectionManager = _CustomSelectionManager(
    onKeyPressed: (keyEvent) {
      final copyPressed =
          (keyEvent.isControlPressed || keyEvent.isMetaPressed) &&
              keyEvent.logicalKey == LogicalKeyboardKey.keyC;

      if (copyPressed) {
        Clipboard.setData(ClipboardData(text: _textToCopy));
        onCopy?.call(_textToCopy);
      }
    },
  );

  var _textToCopy = '';

  static SfDataGridThemeData tableTheme(BuildContext context) {
    final theme = Theme.of(context);

    return SfDataGridThemeData(
      selectionColor: theme.primaryColor.withOpacity(0.1).withBlue(255),
      gridLineStrokeWidth: 1,
      gridLineColor: theme.scaffoldBackgroundColor,
      sortIconColor: theme.iconTheme.color,
      currentCellStyle: DataGridCurrentCellStyle(
        borderWidth: 2,
        borderColor: theme.primaryColor,
      ),
    );
  }

  void onSelectionChanged(List<DataGridRow> added, __) {
    if (tableController.selectedRow == null) return;

    _textToCopy = tableController.selectedRow!
        .getCells()[tableController.currentCell.columnIndex]
        .value
        .toString();
    /*
      row
        .getCells()
        .map((e) => e.value.toString())
        .toList()
        .reduce((value, element) => '$value $element');
     */
  }

  void dispose() {
    customSelectionManager.dispose();
  }
}

class _CustomSelectionManager extends RowSelectionManager {
  _CustomSelectionManager({
    required this.onKeyPressed,
  });

  final Function(RawKeyEvent) onKeyPressed;

  @override
  void handleKeyEvent(RawKeyEvent keyEvent) {
    onKeyPressed(keyEvent);
    super.handleKeyEvent(keyEvent);
  }
}
