import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:modules_basis/modules_basis.dart';

extension DI on BuildContext {
  T readDI<T extends Object>() {
    return read<ContainerDI>()<T>();
  }

  T bloc<T extends StateStreamableSource<Object?>>() {
    return BlocProvider.of<T>(this, listen: false);
  }

  bool get isDarkTheme => Theme.of(this).brightness == Brightness.dark;
}
