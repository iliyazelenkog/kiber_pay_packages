part of 'table_bloc.dart';

class TableState extends Equatable {
  final DynamicTableSortByType sortBy;
  final bool isLoading;
  final bool isColumnFilterPopupOpen;
  final Map<ColumnEntityKeyType, double> columnsWidth;
  final DynamicTableFilterByType<TableFilterJSONImpl> filter;
  // TODO Ilya: переписать на список айди, так как после редактирования основной список данных меняется, а этот нет. После редактирвания перемапить не подходит из-за того что локально в виджетах уже друие данные выбранных
  final List<RowEntity> selectedCheckboxes;
  final List<RowColumnIndex> currentEditingCellLoading;
  final double scale;

  const TableState({
    this.isLoading = false,
    this.isColumnFilterPopupOpen = false,
    this.sortBy = const {},
    this.columnsWidth = const {},
    this.filter = const {},
    this.selectedCheckboxes = const [],
    this.currentEditingCellLoading = const [],
    this.scale = 1.0,
  });

  @override
  List<Object?> get props => [
        sortBy,
        isLoading,
        isColumnFilterPopupOpen,
        columnsWidth,
        filter,
        selectedCheckboxes,
        currentEditingCellLoading,
        scale,
      ];

  TableState copyWith({
    DynamicTableSortByType? sortBy,
    bool? isLoading,
    bool? isColumnFilterPopupOpen,
    DynamicTableFilterByType<TableFilterJSONImpl>? filter,
    Map<ColumnEntityKeyType, double>? columnsWidth,
    List<RowEntity>? selectedCheckboxes,
    List<RowColumnIndex>? currentEditingCellLoading,
    double? scale,
  }) =>
      TableState(
        sortBy: sortBy ?? this.sortBy,
        isLoading: isLoading ?? this.isLoading,
        isColumnFilterPopupOpen:
            isColumnFilterPopupOpen ?? this.isColumnFilterPopupOpen,
        filter: filter ?? this.filter,
        columnsWidth: columnsWidth ?? this.columnsWidth,
        selectedCheckboxes: selectedCheckboxes ?? this.selectedCheckboxes,
        currentEditingCellLoading:
            currentEditingCellLoading ?? this.currentEditingCellLoading,
        scale: scale ?? this.scale,
      );
}
