import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';
import 'package:rxdart/transformers.dart';
import 'package:syncfusion_flutter_datagrid/datagrid.dart';
import 'package:table/domain/entities/column_entity.dart';
import 'package:table/presentation/bloc/events/table_event.dart';
import 'package:table/presentation/bloc/features/table_bloc_feat.dart';

import '../../domain/entities/row_entity.dart';
import '../../domain/repositories/dynamic_table_read_repo.dart';
import '../../infrastructure/filters/list_table_filter_json_impl.dart';
import '../../infrastructure/filters/table_filters_json_impl.dart';
import '../data_source/table_data_grid_source_base.dart';

part 'table_state.dart';

typedef TableOnSortCallback = Future<void> Function(SortTableEvent);

// TODO Ilya: maybe try scale bloc architecture https://itnext.io/flutter-blocs-at-scale-2-keeping-blocs-lean-1b659536e3ec
// @injectable
class TableBloc extends Bloc<TableEvent, TableState> {
  final TableOnSortCallback? onSort;
  final List<TableBlocFeat> features;
  TableDataGridSourceBase tableDataSource;

  TableBloc({
    this.onSort,
    this.features = const [],
    TableState initState = const TableState(),
    required this.tableDataSource,
  }) : super(initState) {
    for (var feat in features) {
      feat.init(bloc: this);
    }

    on<LoadingTableEvent>(_setLoading);
    on<ApplyFilterTableEvent>(_filterApply);
    on<SetFiltersTableEvent>(_setFilters);
    on<SetSortByTableEvent>(_setSortBy);
    on<ColumnFilterPopupOpenStatus>(_setIsColumnFilterPopupOpen);
    on<SortTableEvent>(
      _sort,
      transformer: (events, mapper) => events
          .debounceTime(const Duration(milliseconds: 500))
          .asyncExpand(mapper),
    );
  }

  void _filterApply(
    ApplyFilterTableEvent event,
    emit,
  ) {
    final filter = event.filter;
    final filterKey = event.filterCurrentColumn.key;
    final newFilters = {
      ...state.filter,
      filterKey: filter,
    };

    // Remove filter if null
    if (filter == null) {
      newFilters.remove(filterKey);
    }

    // Remove empty filter
    if (filter is ListTableFilterImpl && filter.selectedItems.isEmpty) {
      newFilters.remove(filterKey);
    }

    emit(state.copyWith(
      filter: newFilters,
    ));
  }

  void _setFilters(
    SetFiltersTableEvent event,
    emit,
  ) {
    emit(state.copyWith(
      filter: event.filters,
    ));
  }

  void _setSortBy(
    SetSortByTableEvent event,
    emit,
  ) {
    emit(state.copyWith(
      sortBy: event.sortBy,
    ));
  }

  void _setIsColumnFilterPopupOpen(
    ColumnFilterPopupOpenStatus event,
    emit,
  ) {
    emit(state.copyWith(
      isColumnFilterPopupOpen: event.isOpen,
    ));
  }

  void _setLoading(LoadingTableEvent event, emit) {
    emit(state.copyWith(
      isLoading: event.isLoading,
    ));
  }

  Future<void> _sort(SortTableEvent event, emit) async {
    emit(state.copyWith(
      sortBy: {
        // TODO Ilya: from event ...state.sortBy,
        event.column: event.sortType,
      },
      isLoading: true,
    ));

    await onSort?.call(event);

    emit(state.copyWith(
      isLoading: false,
    ));
  }

  @override
  Future<void> close() async {
    await Future.wait(
      features.map((e) => e.close()),
    );

    await super.close();
  }
}
