import 'package:flutter/foundation.dart';

import '../table_bloc.dart';

abstract class TableBlocFeat {
  late final TableBloc bloc;

  TableState get state => bloc.state;

  @mustCallSuper
  void init({
    required TableBloc bloc,
  }) {
    this.bloc = bloc;
  }

  Future<void> close() async {}
}
