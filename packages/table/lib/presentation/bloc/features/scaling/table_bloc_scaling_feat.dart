import '../table_bloc_feat.dart';
import 'table_scaling_events.dart';

class TableBlocScalingFeat extends TableBlocFeat {
  @override
  void init({
    required bloc,
  }) {
    super.init(bloc: bloc);

    bloc.on<TableScaleEvent>(_onScale);
  }

  void _onScale(
    TableScaleEvent event,
    emit,
  ) {
    emit(state.copyWith(
      scale: event.value,
    ));
  }
}
