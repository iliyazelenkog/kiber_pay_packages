import '../../events/table_event.dart';

class TableScaleEvent extends TableEvent {
  final double value;

  const TableScaleEvent({
    required this.value,
  });
}
