// import '../../../../domain/entities/column_entity.dart';
// import '../../../../domain/repositories/dynamic_table_filter_by_in_json_type.dart';
// import '../../table_bloc.dart';
//
// TODO Ilya: think about
// class TableResizingState extends TableState {
//   final Map<ColumnEntityKeyType, double?> columnsWidth;
//
//   const TableResizingState({
//     this.columnsWidth = const {},
//     super.sortBy,
//     super.isLoading,
//     super.filterInput,
//     super.filterCurrentItems,
//     super.filter,
//     super.filterCurrentColumn,
//   });
//
//   @override
//   List<Object?> get props => [
//         columnsWidth,
//       ];
//
//   @override
//   TableResizingState copyWith({
//     DynamicTableSortByType? sortBy,
//     bool? isLoading,
//     bool? Function()? filterCurrentBoolValue,
//     List<String>? filterCurrentItems,
//     TableColumnsFilterType? filter,
//     ColumnEntity? filterCurrentColumn,
//     Map<ColumnEntityKeyType, String?>? filterInput,
//     test,
//   }) =>
//       TableResizingState(
//         sortBy: sortBy ?? this.sortBy,
//         isLoading: isLoading ?? this.isLoading,
//         filterCurrentItems: filterCurrentItems ?? this.filterCurrentItems,
//         filter: filter ?? this.filter,
//         filterCurrentColumn: filterCurrentColumn ?? this.filterCurrentColumn,
//         filterInput: filterInput ?? this.filterInput,
//       );
// }
