import 'package:syncfusion_flutter_datagrid/datagrid.dart';

import '../../events/table_event.dart';

class TableResizeEvent extends TableEvent {
  final ColumnResizeUpdateDetails details;

  const TableResizeEvent({
    required this.details,
  });
}
