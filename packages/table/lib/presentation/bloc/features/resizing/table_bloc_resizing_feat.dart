import '../../../mappers/column_entity_to_sf_grid_column_mapper.dart';
import '../table_bloc_feat.dart';
import 'table_resizing_events.dart';

class TableBlocResizingFeat extends TableBlocFeat {
  @override
  void init({
    required bloc,
  }) {
    super.init(bloc: bloc);

    bloc.on<TableResizeEvent>(_onResize);
  }

  void _onResize(
    TableResizeEvent event,
    emit,
  ) {
    try {
      emit(state.copyWith(
        columnsWidth: {
          ...state.columnsWidth,
          event.details.column.columnName: event.details.width,
        },
      ));

      final columnName = event.details.column.columnName;
      final entity = bloc.tableDataSource.columnsEntities
          .firstWhere((col) => col.label == columnName);

      entity.resizeWidth(event.details.width.toInt());

      final index = bloc.tableDataSource.columnsSF
          .indexWhere((col) => col.columnName == columnName);

      // Жаль нельзя просто width поменять, приходится пересоздавать инстанц
      bloc.tableDataSource.columnsSF[index] =
          ColumnEntityToSfGridColumnMapper.mapSingle(entity);
      bloc.tableDataSource.notifyListeners();
    } catch (e, s) {
      print('$e $s');
    }
  }
}
