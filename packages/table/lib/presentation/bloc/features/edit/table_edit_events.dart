import '../../../../domain/entities/column_entity.dart';
import '../../../../domain/entities/row_entity.dart';
import '../../events/table_event.dart';

class EditTableEvent extends TableEvent {
  final RowEntity row;
  final ColumnEntity column;
  final dynamic newValue;
  final dynamic oldValue;

  const EditTableEvent({
    required this.row,
    required this.column,
    required this.newValue,
    required this.oldValue,
  });
}
