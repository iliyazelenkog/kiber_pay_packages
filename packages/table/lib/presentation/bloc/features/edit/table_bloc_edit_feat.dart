import 'dart:async';

import 'package:syncfusion_flutter_datagrid/datagrid.dart';

import '../../../../infrastructure/entities/row_entity_impl.dart';
import '../../../data_source/data_grid_cell_extended.dart';
import '../../../data_source/table_data_grid_source_base.dart';
import '../table_bloc_feat.dart';
import 'table_edit_events.dart';

typedef TableOnEditCallback = Future<void> Function(EditTableEvent);

class TableBlocEditFeat extends TableBlocFeat {
  final TableOnEditCallback? onEdit;

  TableBlocEditFeat({
    this.onEdit,
  });

  @override
  void init({
    required bloc,
  }) {
    super.init(bloc: bloc);

    bloc.on<EditTableEvent>(_onEdit);
  }

  TableDataGridSourceBase get tableDataSource => bloc.tableDataSource;

  // TODO Ilya: update logic to presenter?
  void _onEdit(EditTableEvent event, emit) async {
    final rowIndex = tableDataSource.rowsEntities.indexOf(event.row);
    final cells = tableDataSource.rowsSF[rowIndex].getCells();
    final columnIndex = tableDataSource.columnsEntities.indexOf(event.column);
    final currentCell = cells[columnIndex];
    final rowColumnIndex = RowColumnIndex(rowIndex, columnIndex);

    void change(dynamic value) {
      void createCell<T>() {
        cells[columnIndex] = DataGridCellExtended<T>(
          columnName: currentCell.columnName,
          value: value,
          darkThemeColor: currentCell.darkThemeColor,
          lightThemeColor: currentCell.lightThemeColor,
        );
      }

      try {
        // Apply
        switch (event.newValue.runtimeType) {
          case int:
            createCell<int>();
            break;
          case double:
            createCell<double>();
            break;
          case bool:
            createCell<bool>();
            break;
          case String:
            createCell<String>();
            break;
          case DateTime:
            createCell<DateTime>();
            break;
        }

        final colKey = event.column.key;
        final oldRowEntityField = event.row.fields[colKey]!;

        // Актуализирует сам row в TableDataGridSource
        event.row.fields[colKey] = RowEntityFieldImpl(
          data: value,
          darkThemeColor: oldRowEntityField.darkThemeColor,
          lightThemeColor: oldRowEntityField.lightThemeColor,
        );
        // (tableDataSource as TableDataGridSource).setRows(tableDataSource
        //     .rowsEntities
        //     .map((row) => row.id == event.row.id ? event.row : row)
        //     .toList());

        // dataGridController.refreshRow(rowIndex);
        // tableDataSource.notifyListeners();
        tableDataSource.notifyDataSourceListeners(
          rowColumnIndex: rowColumnIndex,
        );
      } catch (e, s) {
        print('$e $s');
      }
    }

    change(event.newValue);

    emit(state.copyWith(
      currentEditingCellLoading: [
        ...state.currentEditingCellLoading,
        rowColumnIndex
      ],
    ));

    try {
      // TODO Ilya: take new data from backend
      await onEdit?.call(event);
      // await Future.delayed(const Duration(seconds: 3), () {});
    } catch (e, s) {
      // Revert
      change(event.oldValue);

      print('$e $s');

      rethrow;
    } finally {
      emit(state.copyWith(
        // Remove current item from loading
        currentEditingCellLoading: state.currentEditingCellLoading
            .where((e) => e != rowColumnIndex)
            .toList(),
      ));
    }
  }
}
