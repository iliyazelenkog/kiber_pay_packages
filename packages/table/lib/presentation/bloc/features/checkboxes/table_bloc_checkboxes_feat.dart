import '../../../../domain/entities/row_entity.dart';
import '../table_bloc_feat.dart';
import 'table_checkboxes_events.dart';

typedef OnCheckboxesChangedCallback = Function({
  RowEntity? rowEntity,
  bool? value,
  required List<RowEntity> selectedCheckboxes,
});

class TableBlocCheckboxesFeat extends TableBlocFeat {
  final OnCheckboxesChangedCallback? onCheckboxesChanged;

  TableBlocCheckboxesFeat({
    this.onCheckboxesChanged,
  });

  @override
  void init({
    required bloc,
  }) {
    super.init(bloc: bloc);

    bloc.on<ChangeCheckboxEvent>(_onChangeCheckbox);
    bloc.on<SetCheckboxesEvent>(_onSetCheckboxes);
  }

  void _onChangeCheckbox(ChangeCheckboxEvent event, emit) {
    final rowEntity = event.rowEntity;
    final value = event.value;

    if (value) {
      emit(state.copyWith(
        selectedCheckboxes: [...state.selectedCheckboxes, rowEntity],
      ));
    } else {
      emit(state.copyWith(
        selectedCheckboxes:
            state.selectedCheckboxes.where((row) => row != rowEntity).toList(),
      ));
    }

    onCheckboxesChanged?.call(
      rowEntity: rowEntity,
      value: value,
      selectedCheckboxes: state.selectedCheckboxes,
    );
  }

  void _onSetCheckboxes(SetCheckboxesEvent event, emit) {
    emit(state.copyWith(
      selectedCheckboxes: event.checkboxes,
    ));

    onCheckboxesChanged?.call(
      selectedCheckboxes: state.selectedCheckboxes,
    );
  }
}
