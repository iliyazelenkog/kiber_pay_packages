import '../../../../domain/entities/row_entity.dart';
import '../../events/table_event.dart';

class ChangeCheckboxEvent extends TableEvent {
  final RowEntity rowEntity;
  final bool value;

  ChangeCheckboxEvent({
    required this.rowEntity,
    required this.value,
  });
}

class SetCheckboxesEvent extends TableEvent {
  final List<RowEntity> checkboxes;

  SetCheckboxesEvent({
    required this.checkboxes,
  });
}
