import '../../../domain/column_sort_type.dart';
import '../../../domain/entities/column_entity.dart';
import '../../../domain/repositories/dynamic_table_read_repo.dart';
import '../../../infrastructure/filters/table_filters_json_impl.dart';

abstract class TableEvent {
  const TableEvent();
}

class SortTableEvent extends TableEvent {
  final ColumnEntityKeyType column;
  final ColumnSortType? sortType;

  const SortTableEvent({
    required this.column,
    required this.sortType,
  });
}

class LoadingTableEvent extends TableEvent {
  final bool isLoading;

  const LoadingTableEvent({
    required this.isLoading,
  });
}

class ApplyFilterTableEvent extends TableEvent {
  final ColumnEntity filterCurrentColumn;
  final TableFilterJSONImpl? filter;

  const ApplyFilterTableEvent({
    required this.filterCurrentColumn,
    required this.filter,
  });
}

class SetFiltersTableEvent extends TableEvent {
  final DynamicTableFilterByType<TableFilterJSONImpl> filters;

  const SetFiltersTableEvent({
    required this.filters,
  });
}

class SetSortByTableEvent extends TableEvent {
  final DynamicTableSortByType sortBy;

  const SetSortByTableEvent({
    required this.sortBy,
  });
}

class ColumnFilterPopupOpenStatus extends TableEvent {
  final bool isOpen;

  ColumnFilterPopupOpenStatus({
    required this.isOpen,
  });
}
