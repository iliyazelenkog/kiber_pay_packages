import 'dart:async';

import 'package:collection/collection.dart';
import 'package:flutter/services.dart';
import 'package:syncfusion_flutter_datagrid/datagrid.dart';
import 'package:table/domain/entities/row_entity.dart';

import '../domain/entities/column_entity.dart';
import '../domain/exceptions/edit_row_column_not_found.dart';
import '../domain/repositories/dynamic_table_read_repo.dart';
import '../infrastructure/filters/table_filters_json_impl.dart';
import 'bloc/events/table_event.dart';
import 'bloc/features/scaling/table_scaling_events.dart';
import 'bloc/table_bloc.dart';
import 'data_source/data_grid_row_extended.dart';
import 'data_source/table_data_grid_source.dart';

class TableWidgetController {
  late TableBloc _tableBloc;

  final _blocCompleter = Completer<TableBloc>();

  Future<TableBloc> get tableBlocFuture => _blocCompleter.future;

  Future<TableState> get tableStateFuture =>
      tableBlocFuture.then((value) => value.state);

  TableDataGridSource get tableDataSource =>
      _tableBloc.tableDataSource as TableDataGridSource;

  void init({
    required TableBloc tableBloc,
  }) {
    _tableBloc = tableBloc;

    // isCompleted because "Future already completed" sometimes
    if (!_blocCompleter.isCompleted) {
      _blocCompleter.complete(_tableBloc);
    }
  }

  void scale(double value) async {
    (await tableBlocFuture).add(TableScaleEvent(value: value));
  }

  String copyRows(List<RowEntity> rows) {
    final text = rows.map((row) => _getRowEntityText(row)).join('\n');

    Clipboard.setData(ClipboardData(text: text));

    return text;
  }

  void setFilterItems(DynamicTableFilterByType<TableFilterJSONImpl> filters) {
    _tableBloc.add(SetFiltersTableEvent(filters: filters));
  }

  Future<void> sendCellEdit({
    required DataGridRowExtended rowSF,
    // TODO Ilya: try extended
    required GridColumn columnsSF,
    required dynamic newValue,
  }) async {
    final rowIndex = tableDataSource.rowsSF.indexOf(rowSF);
    final columnIndex = tableDataSource.columnsSF.indexOf(
      tableDataSource.getExtendedSFColumnByNotExtended(columnsSF)!,
    );
    final rowColumnIndex = RowColumnIndex(rowIndex, columnIndex);

    tableDataSource
      ..setEditCellValue(newValue)
      ..onCellSubmit(
        rowSF,
        rowColumnIndex,
        columnsSF,
      );

    if (_tableBloc.state.currentEditingCellLoading
        .any((e) => e.equals(rowColumnIndex))) {
      // Waiting end of editing loading
      await _tableBloc.stream.firstWhere((element) => !element
          .currentEditingCellLoading
          .any((e) => e.equals(rowColumnIndex)));
    }
  }

  Future<void> editRowsByColumn({
    required ColumnEntityKeyType columnKey,
    required List<RowEntity> rows,
    required dynamic newValue,
  }) async {
    final columnsSF = tableDataSource.columnsSF.firstWhereOrNull(
      (element) => element.key == columnKey,
    );

    if (columnsSF == null) {
      throw EditRowColumnNotFound(columnKey);
    }

    final futures = <Future>[];

    for (var row in rows) {
      final rowSF = tableDataSource.getRowSFByEntity(row);
      // Актуальный, так как мог быть старый (до редактирования, со старым data)
      final actualRow =
          tableDataSource.rowsEntities.firstWhere((e) => e.id == row.id);

      // If already has this value
      if (actualRow.fields[columnKey]!.data == newValue) {
        continue;
      }

      futures.add(sendCellEdit(
        rowSF: rowSF,
        columnsSF: columnsSF,
        newValue: newValue,
      ));
    }

    await Future.wait(futures);
  }

  String _getRowEntityText(RowEntity rowEntity) {
    final row = tableDataSource.getRowSFByEntity(rowEntity);

    return row
        .getCells()
        .map((e) => e.value.toString())
        .toList()
        .reduce((prev, curr) => '$prev $curr');
  }
}
