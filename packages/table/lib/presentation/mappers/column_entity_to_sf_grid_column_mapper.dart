import 'package:flutter/cupertino.dart';
import 'package:syncfusion_flutter_datagrid/datagrid.dart';

import '../../domain/entities/column_entity.dart';
import '../column/column_widget.dart';
import '../data_source/grid_column_extended.dart';

abstract class ColumnEntityToSfGridColumnMapper {
  static List<GridColumnExtended> map(List<ColumnEntity> columns) =>
      columns.map(mapSingle).toList();

  static GridColumnExtended mapSingle(ColumnEntity column) =>
      GridColumnExtended(
        key: column.key,
        labelText: column.label,
        visible: column.visible,
        allowEditing: column.editable,
        allowFiltering: column.filterable,
        allowSorting: column.sortable,
        columnWidthMode: column.autoWidth
            ? ColumnWidthMode.auto
            : ColumnWidthMode.fitByColumnName,
        // Ignore column.autoWidth! column.width is important!
        width: column.width == null ? double.nan : column.width!.toDouble(),
        // minimumWidth: column.minimumWidth.toDouble(),
        // TODO Ilya: don't duplicate in [TableColumnSizer.computeCellWidth]
        autoFitPadding: const EdgeInsets.symmetric(vertical: 8),
        filterIconPadding: EdgeInsets.zero,
        labelWidget: ColumnWidget(
          key: ObjectKey(column),
          columnEntity: column,
        ),
      );
}
