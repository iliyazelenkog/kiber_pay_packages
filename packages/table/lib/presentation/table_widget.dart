import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:provider/provider.dart';
import 'package:syncfusion_flutter_core/theme.dart';
import 'package:syncfusion_flutter_datagrid/datagrid.dart';
import 'package:table/presentation/formatters/table_cell_formatter_impl.dart';
import 'package:table/presentation/table_column_sizer.dart';
import 'package:table/presentation/table_widget_controller.dart';
import 'package:table/table.dart';

import '../domain/entities/column_entity.dart';
import '../domain/entities/row_entity.dart';
import '../domain/repositories/dynamic_table_read_repo.dart';
import '../infrastructure/filters/table_filters_json_impl.dart';
import 'bloc/events/table_event.dart';
import 'bloc/features/checkboxes/table_bloc_checkboxes_feat.dart';
import 'bloc/features/checkboxes/table_checkboxes_events.dart';
import 'bloc/features/edit/table_bloc_edit_feat.dart';
import 'bloc/features/resizing/table_bloc_resizing_feat.dart';
import 'bloc/features/resizing/table_resizing_events.dart';
import 'bloc/features/scaling/table_bloc_scaling_feat.dart';
import 'bloc/table_bloc.dart';
import 'data_source/table_data_grid_source.dart';
import 'data_source/table_data_grid_source_base.dart';
import 'formatters/table_cell_formatter.dart';
import 'table_widget_presenter.dart';

/*
  Виджет таблцы основаный на syncfusion flutter пакете.
 */
class TableWidget extends StatefulWidget {
  final List<ColumnEntity> columns;
  final List<RowEntity> rows;
  final DynamicTableFilterByType<TableFilterJSONImpl>? filters;
  final DynamicTableSortByType? sortBy;
  // Метод который возвращает элементы для фильтра.
  final FilterItemsResolverCallback? filterItemsResolver;
  // Колбек вызывается при изменении фильтрации.
  final void Function(DynamicTableFilterByType<TableFilterJSONImpl>)?
      onFilterChange;
  final TableOnSortCallback? onSort;
  final TableOnEditCallback? onEdit;
  final OnCopyCallback? onCopy;
  final OnCheckboxesChangedCallback? onCheckboxesChanged;
  final void Function(ColumnResizeUpdateDetails details)? onColumnResizeUpdate;
  final SelectionMode selectionMode;
  final GridNavigationMode navigationMode;
  // Форматтер ячеек таблицы.
  final TableCellFormatter tableCellFormatter;
  // Контроллер позволяющий взаимодействовать с внутреностями таблицы.
  final TableWidgetController? controller;
  final int frozenColumnsCount;
  final bool isLoading, showLoadingIndicator, highlightFooter;

  const TableWidget({
    super.key,
    required this.columns,
    required this.rows,
    this.filters,
    this.sortBy,
    this.selectionMode = SelectionMode.single,
    this.navigationMode = GridNavigationMode.cell,
    this.onSort,
    this.onEdit,
    this.onCopy,
    this.onCheckboxesChanged,
    this.onColumnResizeUpdate,
    this.onFilterChange,
    this.tableCellFormatter = const TableCellFormatterImpl(),
    this.controller,
    this.filterItemsResolver,
    this.frozenColumnsCount = 0,
    this.isLoading = false,
    this.showLoadingIndicator = true,
    this.highlightFooter = false,
  });

  @override
  State<TableWidget> createState() => _TableWidgetState();
}

class _TableWidgetState extends State<TableWidget> {
  final tableController = DataGridController();

  late final data = TableDataGridSource(
    columnsEntities: widget.columns,
    rowsEntities: widget.rows,
  );
  late final _presenter = TableWidgetPresenter(
    tableController: tableController,
    highlightFooter: widget.highlightFooter,
    onCopy: _onCopy,
  );
  late final TableBloc _tableBloc = TableBloc(
    onSort: widget.onSort,
    initState: TableState(
      filter: widget.filters ?? const {},
      sortBy: widget.sortBy ?? const {},
      isLoading: widget.isLoading,
    ),
    tableDataSource: data,
    // TODO Ilya: pass directly through constructor? (open/closed principle)
    features: [
      TableBlocResizingFeat(),
      TableBlocEditFeat(
        onEdit: widget.onEdit,
      ),
      TableBlocCheckboxesFeat(
        onCheckboxesChanged: widget.onCheckboxesChanged,
      ),
      TableBlocScalingFeat(),
    ],
  );

  void _onCopy(String copiedText) {
    widget.onCopy?.call(context, copiedText);
  }

  void _initDataSource() {
    data.init(
      bloc: _tableBloc,
      // ctx: context,
    );
    // TODO Ilya: think about better solution
    _tableBloc.tableDataSource = data;
  }

  // Когда пользователь нажимает на ячейку, то сначала вызывается этот метод
  bool _onCurrentCellActivating(
    RowColumnIndex newRowColumnIndex,
    RowColumnIndex oldRowColumnIndex,
  ) {
    final col = data.columnsEntities[newRowColumnIndex.columnIndex];

    if (col.editable && col.type == ColumnType.bool) {
      // Cancel selection for bool switch
      return false;
    }

    return true;
  }

  @override
  void initState() {
    super.initState();

    widget.tableCellFormatter.init();
    _initDataSource();
    widget.controller?.init(tableBloc: _tableBloc);
  }

  @override
  void didUpdateWidget(covariant TableWidget oldWidget) {
    super.didUpdateWidget(oldWidget);

    if (!listEquals(oldWidget.columns, widget.columns)) {
      data
        ..setColumns(widget.columns)
        // Also set rows to use updated displayed columns
        ..setRows(widget.rows)
        ..notifyListeners();
    }
    if (!listEquals(oldWidget.rows, widget.rows)) {
      data
        ..setRows(widget.rows)
        ..notifyListeners();

      // Reset checkboxes when rows changed
      // TODO Ilya: maybe remove only not existing rows at the moment?
      _tableBloc.add(SetCheckboxesEvent(
        checkboxes: [],
      ));
    }

    if (oldWidget.isLoading != widget.isLoading) {
      _tableBloc.add(LoadingTableEvent(
        isLoading: widget.isLoading,
      ));
    }
    // TODO Ilya: uncomment after fix recursive callback issue
    // if (oldWidget.filters != widget.filters) {
    //   _applyFilters(widget.filters!);
    // }
    // if (oldWidget.sortBy != widget.sortBy) {
    //   _applySortBy(widget.sortBy!);
    // }
  }

  @override
  Widget build(BuildContext context) {
    final theme = Theme.of(context);

    return MultiProvider(
      providers: [
        Provider<TableCellFormatter>(
          create: (_) => widget.tableCellFormatter,
        ),
        Provider<TableWidgetPresenter>(
          create: (_) => _presenter,
        ),
        Provider<TableWidgetController?>(
          create: (_) => widget.controller,
        ),
        ChangeNotifierProvider<TableDataGridSourceBase>(
          create: (_) => data,
        ),
        if (widget.filterItemsResolver != null)
          Provider<FilterItemsResolverCallback>(
            create: (_) => widget.filterItemsResolver!,
          ),
      ],
      child: BlocProvider(
        create: (context) => _tableBloc,
        child: BlocListener<TableBloc, TableState>(
          listenWhen: (prev, curr) => prev.filter != curr.filter,
          listener: (context, state) {
            widget.onFilterChange?.call(state.filter);
          },
          child: Column(
            children: [
              if (widget.showLoadingIndicator)
                // Rebuild on loading
                BlocBuilder<TableBloc, TableState>(
                  buildWhen: (previous, current) =>
                      previous.isLoading != current.isLoading,
                  builder: (context, state) => state.isLoading
                      ? const LinearProgressIndicator()
                      : const SizedBox.shrink(),
                ),
              Expanded(
                child: SfDataGridTheme(
                  data: TableWidgetPresenter.tableTheme(context),
                  // Пробовал разные способы не ребилдить всё: не получилось. Например, менять SizedBox в ColumnWidget, но GridColumn не позволяет.
                  child: BlocBuilder<TableBloc, TableState>(
                    buildWhen: (p, c) =>
                        p.columnsWidth != c.columnsWidth || p.scale != c.scale,
                    builder: (context, state) => SfDataGrid(
                      controller: _presenter.tableController,
                      isScrollbarAlwaysShown: true,
                      highlightRowOnHover: false,
                      allowEditing: true,

                      // Resizing
                      allowColumnsResizing: true,
                      columnResizeMode: ColumnResizeMode.onResize,
                      onColumnResizeUpdate:
                          (ColumnResizeUpdateDetails details) {
                        _tableBloc.add(TableResizeEvent(details: details));
                        widget.onColumnResizeUpdate?.call(details);
                        return true;
                      },

                      columnSizer: TableColumnSizer(
                        scale: state.scale,
                        tableCellFormatter: widget.tableCellFormatter,
                        dataSource: data,
                        // TODO Ilya: no duplicates (maybe provide table theme with this styles)
                        headerTextStyle: theme.textTheme.bodyLarge,
                        // TODO Ilya: duplicates with RowCellWidget!
                        cellTextStyle: theme.textTheme.bodyLarge,
                      ),
                      gridLinesVisibility: GridLinesVisibility.both,
                      headerGridLinesVisibility: GridLinesVisibility.both,
                      editingGestureType: EditingGestureType.doubleTap,
                      selectionMode: widget.selectionMode,
                      onCurrentCellActivating: _onCurrentCellActivating,

                      // Data
                      source: data,
                      columns: data.columnsSF,

                      navigationMode: widget.navigationMode,
                      selectionManager: _presenter.customSelectionManager,
                      onSelectionChanged: _presenter.onSelectionChanged,
                      frozenColumnsCount: widget.frozenColumnsCount,
                    ),
                  ),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }

  @override
  dispose() {
    _presenter.dispose();
    _tableBloc.close();
    tableController.dispose();

    super.dispose();
  }
}
