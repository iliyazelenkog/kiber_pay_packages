import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:table/presentation/bloc/table_bloc.dart';

class RowCellEditSwitch extends StatelessWidget {
  final dynamic value;
  final ValueChanged<bool> onChanged;
  final bool isLoading;

  const RowCellEditSwitch({
    super.key,
    required this.value,
    required this.onChanged,
    this.isLoading = false,
  });

  static const width = 60.0;

  @override
  Widget build(BuildContext context) => BlocBuilder<TableBloc, TableState>(
        builder: (context, state) => Transform.scale(
          scale: state.scale,
          child: SizedBox(
            width: width,
            child: Switch.adaptive(
              value: value,
              onChanged: isLoading ? null : onChanged,
            ),
          ),
        ),
      );
}
