import 'package:collection/collection.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:syncfusion_flutter_datagrid/datagrid.dart';
import 'package:table/domain/entities/row_entity.dart';
import 'package:table/presentation/extensions/di_on_build_context.dart';
import 'package:table/presentation/formatters/table_cell_formatter.dart';
import 'package:table/presentation/table_widget_controller.dart';

import '../../domain/entities/column_entity.dart';
import '../bloc/features/checkboxes/table_checkboxes_events.dart';
import '../bloc/table_bloc.dart';
import '../column/custom_column_entity_impl.dart';
import '../data_source/data_grid_cell_extended.dart';
import '../data_source/table_data_grid_source.dart';
import '../data_source/table_data_grid_source_base.dart';
import '../table_widget_presenter.dart';
import 'edit/row_cell_edit_switch.dart';

class RowCellWidget extends StatelessWidget {
  final ColumnEntity column;
  final RowEntity row;
  final DataGridCellExtended cell;

  const RowCellWidget({
    super.key,
    required this.column,
    required this.row,
    required this.cell,
  });

  @override
  Widget build(BuildContext context) {
    final theme = Theme.of(context);
    final bloc = context.bloc<TableBloc>();
    final cellFormatter = context.watch<TableCellFormatter>();
    final dataSource =
        context.watch<TableDataGridSourceBase>() as TableDataGridSource;
    final presenter = context.watch<TableWidgetPresenter>();
    final controller = context.watch<TableWidgetController>();
    final rowIndex = dataSource.rowsEntities.indexOf(row);
    final colIndex = dataSource.columnsEntities.indexOf(column);
    // TODO Ilya: backend
    final isFooter = presenter.highlightFooter &&
        rowIndex == dataSource.rowsEntities.length - 1;
    final value = cell.value;

    Widget child = (value is String && value.isEmpty)
        ? const SizedBox.shrink()
        : Padding(
            padding: const EdgeInsets.symmetric(horizontal: 8),
            child: Align(
              alignment: Alignment.centerLeft,
              child: BlocBuilder<TableBloc, TableState>(
                buildWhen: (p, c) => p.scale != c.scale,
                builder: (_, state) => Text(
                  cellFormatter.format(value, column),
                  style: theme.textTheme.bodyLarge
                      ?.merge(
                        TextStyle(
                          fontWeight:
                              isFooter ? FontWeight.bold : FontWeight.normal,
                        ),
                      )
                      .apply(
                        fontSizeFactor: state.scale,
                      ),
                ),
              ),
            ),
          );

    if (column.type == ColumnType.bool && column.editable) {
      child = RowCellEditSwitch(
        // Can be null from backend
        value: value ?? false,
        onChanged: (bool newValue) {
          final rowIndex = dataSource.rowsEntities.indexOf(row);
          final dataGridRow = dataSource.rowsSF[rowIndex];
          // final columnIndex = dataSource.columnsEntities.indexOf(column);
          final dataGridColumn =
              dataSource.columnsSF.firstWhere((e) => e.key == column.key);

          controller.sendCellEdit(
            columnsSF: dataGridColumn,
            rowSF: dataGridRow,
            newValue: newValue,
          );
        },
      );
    } else if (column.type == ColumnType.checkbox) {
      child = BlocBuilder<TableBloc, TableState>(
        buildWhen: (p, c) => p.selectedCheckboxes != c.selectedCheckboxes,
        builder: (context, state) => Checkbox(
          value: state.selectedCheckboxes.contains(row),
          onChanged: (bool? newValue) {
            bloc.add(ChangeCheckboxEvent(
              rowEntity: row,
              value: newValue ?? false,
            ));
          },
        ),
      );
    } else if (column.type == ColumnType.custom &&
        column is CustomColumnEntityImpl) {
      child = (column as CustomColumnEntityImpl).builder.call(
            context,
            CustomColumnBuilderInfo(
              column: column,
              row: row,
              cell: cell,
              dataSource: dataSource,
            ),
          );
    }

    final color =
        context.isDarkTheme ? cell.darkThemeColor : cell.lightThemeColor;

    return BlocBuilder<TableBloc, TableState>(
      buildWhen: (p, c) =>
          p.currentEditingCellLoading != c.currentEditingCellLoading,
      builder: (context, state) {
        final isCurrentCellLoading = state.currentEditingCellLoading
                .firstWhereOrNull((e) =>
                    e.equals(RowColumnIndex(rowIndex, colIndex)) == true) !=
            null;

        return Stack(
          children: [
            if (isCurrentCellLoading)
              const Positioned(
                child: Center(
                  child: CircularProgressIndicator(),
                ),
              ),
            Positioned.fill(
              child: Opacity(
                opacity: isCurrentCellLoading ? 0.3 : 1,
                child: color == null
                    ? child
                    : ColoredBox(
                        color: color,
                        child: child,
                      ),
              ),
            ),
          ],
        );
      },
    );
  }
}
