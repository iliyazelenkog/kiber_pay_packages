import 'package:table/domain/entities/column_entity.dart';

abstract class TableCellFormatter {
  const TableCellFormatter();

  void init();

  String format(
    dynamic value,
    ColumnEntity column,
  );
}
