import 'package:intl/intl.dart';
import 'package:intl/number_symbols.dart';
import 'package:intl/number_symbols_data.dart';

import '../../domain/entities/column_entity.dart';
import 'table_cell_formatter.dart';

class TableCellFormatterImpl extends TableCellFormatter {
  const TableCellFormatterImpl();

  static const _customFormatterKey = 'kiber_table';

  @override
  init() {
    numberFormatSymbols[_customFormatterKey] = const NumberSymbols(
      NAME: _customFormatterKey,
      DECIMAL_SEP: '.',
      GROUP_SEP: ' ',
      PERCENT: '%',
      ZERO_DIGIT: '0',
      PLUS_SIGN: '+',
      MINUS_SIGN: '-',
      EXP_SYMBOL: 'e',
      PERMILL: '\u2030',
      INFINITY: '\u221E',
      NAN: 'NaN',
      DECIMAL_PATTERN: '#,##0.###',
      SCIENTIFIC_PATTERN: '#E0',
      PERCENT_PATTERN: '#,##0%',
      CURRENCY_PATTERN: '###,###,###.00',
      DEF_CURRENCY_CODE: 'AUD',
    );
  }

  @override
  String format(
    dynamic value,
    ColumnEntity column,
  ) {
    if (column.type == ColumnType.money && value is num) {
      if (value == 0) return '0';

      return NumberFormat('###,###,###.00', _customFormatterKey).format(value);
    }

    switch (value.runtimeType) {
      case Null:
        return '';
      case double:
      case int:
        return _formatNum(value);
      case DateTime:
        return _formatDate(value);
      default:
        return value.toString();
    }
  }

  String _formatDate(DateTime date) {
    var format = 'dd.MM.yyyy';

    if (date.hour != 0 || date.minute != 0) {
      format += ' HH:mm';

      if (date.second != 0) {
        format += ':ss';
      }
    }

    return DateFormat(format).format(date);
  }

  String _formatNum(num value) {
    return NumberFormat('###,###,###.##', _customFormatterKey).format(value);
  }
}
