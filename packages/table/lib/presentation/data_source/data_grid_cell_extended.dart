import 'dart:ui';

import 'package:syncfusion_flutter_datagrid/datagrid.dart';

class DataGridCellExtended<T> extends DataGridCell<T> {
  final Color? lightThemeColor;
  final Color? darkThemeColor;

  const DataGridCellExtended({
    required super.columnName,
    required super.value,
    required this.lightThemeColor,
    required this.darkThemeColor,
  });
}

/*
class DataGridCellExtended<T> extends DataGridCell<T> {
  final String key;
  final String text;
  final Color? lightThemeColor;
  final Color? darkThemeColor;

  const DataGridCellExtended({
    required super.value,
    required this.key,
    required this.text,
    required this.lightThemeColor,
    required this.darkThemeColor,
  }) : super(
          columnName: text,
        );
}
 */
