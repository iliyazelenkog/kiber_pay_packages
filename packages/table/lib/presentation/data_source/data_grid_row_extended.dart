import 'package:syncfusion_flutter_datagrid/datagrid.dart';
import 'package:table/presentation/data_source/data_grid_cell_extended.dart';

class DataGridRowExtended extends DataGridRow {
  const DataGridRowExtended({
    required List<DataGridCellExtended> cells,
  })  : _cells = cells,
        super(cells: cells);

  final List<DataGridCellExtended> _cells;

  @override
  List<DataGridCellExtended> getCells() {
    return _cells;
  }
}
