import 'package:collection/collection.dart';
import 'package:flutter/widgets.dart';
import 'package:syncfusion_flutter_datagrid/datagrid.dart';
import 'package:table/presentation/data_source/table_data_grid_source_base.dart';

import '../../domain/entities/column_entity.dart';
import '../../domain/entities/row_entity.dart';
import '../../infrastructure/mappers/row_entity_to_sf_data_grid_row_mapper.dart';
import '../cell/row_cell_widget.dart';
import '../mappers/column_entity_to_sf_grid_column_mapper.dart';
import 'data_grid_cell_extended.dart';
import 'data_grid_row_extended.dart';
import 'grid_column_extended.dart';
import 'table_data_grid_source_editing_mixin.dart';

class TableDataGridSource extends TableDataGridSourceBase
    with TableDataGridSourceEditingMixin {
  @override
  List<ColumnEntity> columnsEntities;

  @override
  List<RowEntity> rowsEntities;

  TableDataGridSource({
    required this.columnsEntities,
    required this.rowsEntities,
  }) {
    setColumns(columnsEntities);
    setRows(rowsEntities);
  }

  @override
  late List<DataGridRowExtended> rowsSF;

  @override
  late List<GridColumnExtended> columnsSF;

  DataGridRowExtended getRowSFByEntity(RowEntity rowEntity) =>
      rowsEntities.contains(rowEntity)
          ? rowsSF[rowsEntities.indexOf(rowEntity)]
          : throw Exception('Row not found');

  GridColumnExtended? getExtendedSFColumnByNotExtended(GridColumn columnSF) =>
      columnsSF.firstWhereOrNull((e) => e.columnName == columnSF.columnName);

  void setRows(List<RowEntity> rowsInput) {
    rowsEntities = rowsInput;
    rowsSF = RowEntityToSFDataGridRowMapper.map(rowsEntities, columnsEntities);
  }

  void setColumns(List<ColumnEntity> columnsInput) {
    columnsEntities = columnsInput;
    columnsSF = ColumnEntityToSfGridColumnMapper.map(columnsEntities);
  }

  @override
  DataGridRowAdapter? buildRow(covariant DataGridRowExtended row) {
    final rowEntity = rowsEntities[rowsSF.indexOf(row)];

    return DataGridRowAdapter(
      cells: row.getCells().map<Widget>(
        (DataGridCellExtended cell) {
          final key = ObjectKey(cell);
          final columnEntity = columnsEntities
              .firstWhere((column) => column.label == cell.columnName);

          return RowCellWidget(
            key: key,
            column: columnEntity,
            row: rowEntity,
            cell: cell,
          );
        },
      ).toList(),
    );
  }
}
