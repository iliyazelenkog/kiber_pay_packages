import 'package:syncfusion_flutter_datagrid/datagrid.dart';
import 'package:table/presentation/data_source/data_grid_row_extended.dart';

import '../../domain/entities/column_entity.dart';
import '../../domain/entities/row_entity.dart';
import '../bloc/table_bloc.dart';
import 'table_data_grid_source_base_sorting_mixin.dart';

abstract class TableDataGridSourceBase extends DataGridSource
    with TableDataGridSourceBaseSortingMixin {
  bool initiated = false;

  List<DataGridRowExtended> get rowsSF;

  @override
  List<DataGridRowExtended> get rows => rowsSF;

  List<GridColumn> get columnsSF;

  late List<ColumnEntity> columnsEntities;

  late List<RowEntity> rowsEntities;

  late TableBloc tableBloc;
  // late BuildContext context;

  void init({
    required TableBloc bloc,
    // required BuildContext ctx,
  }) {
    if (initiated) return;

    tableBloc = bloc;
    // context = ctx;
    initiated = true;
  }
}
