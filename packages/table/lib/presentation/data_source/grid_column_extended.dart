import 'package:flutter/cupertino.dart';
import 'package:syncfusion_flutter_datagrid/datagrid.dart';
import 'package:table/domain/entities/column_entity.dart';

class GridColumnExtended extends GridColumn {
  final Widget labelWidget;
  final ColumnEntityKeyType key;
  final String labelText;

  GridColumnExtended({
    required this.labelWidget,
    required this.key,
    required this.labelText,
    super.columnWidthMode,
    super.visible,
    super.allowSorting,
    super.autoFitPadding,
    super.minimumWidth,
    super.maximumWidth,
    super.width,
    super.allowEditing,
    super.allowFiltering,
    super.filterPopupMenuOptions,
    super.filterIconPadding,
  }) : super(
          label: labelWidget,
          columnName: labelText,
        );
}
