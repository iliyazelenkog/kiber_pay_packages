import 'package:collection/collection.dart';
import 'package:syncfusion_flutter_datagrid/datagrid.dart';

mixin TableDataGridSourceBaseSortingMixin on DataGridSource {
  @override
  int compare(DataGridRow? a, DataGridRow? b, SortColumnDetails sortColumn) {
    Object? getCellValue(List<DataGridCell>? cells, String columnName) {
      return cells
          ?.firstWhereOrNull(
              (DataGridCell element) => element.columnName == columnName)
          ?.value;
    }

    onSortChanged(sortColumn);
    final Object? valueA = getCellValue(a?.getCells(), sortColumn.name);
    final Object? valueB = getCellValue(b?.getCells(), sortColumn.name);
    return compareTo(valueA, valueB, sortColumn.sortDirection);
  }

  onSortChanged(SortColumnDetails sortColumn) {}

  int compareTo(
      dynamic value1, dynamic value2, DataGridSortDirection sortDirection) {
    if (value1.runtimeType == bool && value2.runtimeType == bool) {
      return compareBool(value1, value2, sortDirection);
    }
    if (sortDirection == DataGridSortDirection.ascending) {
      if (value1 == null) {
        return -1;
      } else if (value2 == null) {
        return 1;
      }
      return value1.compareTo(value2) as int;
    } else {
      if (value1 == null) {
        return 1;
      } else {
        if (value2 == null) {
          return -1;
        }
      }
      return value2.compareTo(value1) as int;
    }
  }

  int compareBool(bool a, bool b, DataGridSortDirection sortDirection) {
    if (sortDirection == DataGridSortDirection.ascending) {
      if (a == b) {
        return 0;
      } else {
        if (a) {
          return 1;
        } else {
          return -1;
        }
      }
    } else {
      if (a == b) {
        return 0;
      } else {
        if (a) {
          return -1;
        } else {
          return 1;
        }
      }
    }
  }
}
