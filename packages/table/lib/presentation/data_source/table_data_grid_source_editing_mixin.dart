import 'package:collection/collection.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:syncfusion_flutter_datagrid/datagrid.dart';
import 'package:table/presentation/data_source/data_grid_row_extended.dart';

import '../../domain/entities/column_entity.dart';
import '../bloc/features/edit/table_edit_events.dart';
import '../bloc/table_bloc.dart';
import 'table_data_grid_source.dart';
import 'table_data_grid_source_base.dart';

mixin TableDataGridSourceEditingMixin on TableDataGridSourceBase {
  TableDataGridSource get _self => this as TableDataGridSource;

  final TextEditingController _editingController = TextEditingController();

  dynamic _newCellValue;

  void setEditCellValue(dynamic value) {
    _newCellValue = value;
  }

  @override
  void onCellSubmit(
    covariant DataGridRowExtended dataGridRow,
    RowColumnIndex rowColumnIndex,
    GridColumn column,
  ) {
    try {
      final newValue = _newCellValue;
      final dynamic oldValue = dataGridRow
              .getCells()
              .firstWhereOrNull((DataGridCell dataGridCell) =>
                  dataGridCell.columnName == column.columnName)
              ?.value ??
          '';

      if (newValue == null || oldValue == newValue) {
        return;
      }

      final int dataRowIndex = rowsSF.indexOf(dataGridRow);
      // final cells = rows[dataRowIndex].getCells();
      // final currentCell = cells[rowColumnIndex.columnIndex];
      // cells[rowColumnIndex.columnIndex] = DataGridCell<int>(
      //   columnName: currentCell.columnName,
      //   value: newValue,
      // );

      // TODO Ilya: presenter?
      final bloc = _self.tableBloc;
      final columnEntity =
          _self.columnsEntities.firstWhere((e) => e.label == column.columnName);
      final row = _self.rowsEntities[dataRowIndex];

      bloc.add(EditTableEvent(
        column: columnEntity,
        row: row,
        newValue: newValue,
        oldValue: oldValue,
      ));

      // Reset
      _newCellValue = null;
    } catch (e) {
      print(e);
    }
  }

  @override
  Widget? buildEditWidget(
    covariant DataGridRowExtended dataGridRow,
    RowColumnIndex rowColumnIndex,
    GridColumn column,
    CellSubmit submitCell,
  ) {
    final String displayText = dataGridRow
            .getCells()
            .firstWhere((DataGridCell dataGridCell) =>
                dataGridCell.columnName == column.columnName)
            .value
            ?.toString() ??
        '';

    _editingController.text = displayText;

    final columnEntity =
        _self.columnsEntities.firstWhere((e) => e.label == column.columnName);
    // final theme = Theme.of(_self.context);

    TextInputType? keyboardType;

    switch (columnEntity.type) {
      case ColumnType.bool:
        // Exit if bool (we always display Switch)
        return null;
      case ColumnType.string:
      case ColumnType.money:
        keyboardType = TextInputType.text;
        break;
      case ColumnType.num:
        keyboardType = TextInputType.number;
        break;
      case ColumnType.float:
        keyboardType = TextInputType.number;
        break;
      case ColumnType.date:
        keyboardType = TextInputType.datetime;
        break;
      default:
        keyboardType = TextInputType.text;
        break;
    }

    return Builder(
      builder: (context) {
        final theme = Theme.of(context);

        return BlocBuilder<TableBloc, TableState>(
          builder: (context, state) => TextField(
            // TODO Ilya: based on column type
            // inputFormatters: [
            //   ThousandsFormatter(
            //       allowFraction: true,
            //       formatter: NumberFormat('###,###.###', Utils.customFormatterKey))
            // ],
            expands: true,
            style: theme.textTheme.bodyMedium
                ?.copyWith(
                  color: theme.colorScheme.primary,
                )
                .apply(fontSizeFactor: state.scale),
            decoration: const InputDecoration(
              border: InputBorder.none,
              filled: true,
              contentPadding: EdgeInsets.symmetric(horizontal: 8, vertical: 0),
            ),
            maxLines: null,
            autofocus: true,
            controller: _editingController,
            keyboardType: keyboardType,
            onChanged: (String value) {
              if (value.isNotEmpty) {
                if (columnEntity.type == ColumnType.float ||
                    columnEntity.type == ColumnType.num) {
                  _newCellValue = int.parse(value);
                } else {
                  _newCellValue = value;
                }
              } else {
                _newCellValue = null;
              }
            },
            onSubmitted: (String value) => submitCell(),
          ),
        );
      },
    );
  }
}
