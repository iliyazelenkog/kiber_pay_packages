import 'package:flutter/painting.dart';
import 'package:syncfusion_flutter_datagrid/datagrid.dart';

import '../domain/entities/column_entity.dart';
import 'cell/edit/row_cell_edit_switch.dart';
import 'column/column_widget.dart';
import 'data_source/table_data_grid_source.dart';
import 'formatters/table_cell_formatter.dart';

class TableColumnSizer extends ColumnSizer {
  final double scale;
  final TextStyle? headerTextStyle;
  final TextStyle? cellTextStyle;
  final TableDataGridSource dataSource;
  final TableCellFormatter tableCellFormatter;

  TableColumnSizer({
    required this.scale,
    required this.headerTextStyle,
    required this.cellTextStyle,
    required this.dataSource,
    required this.tableCellFormatter,
  });

  TextStyle? get headerTextStyleWithScale =>
      headerTextStyle?.apply(fontSizeFactor: scale);

  TextStyle? get cellTextStyleWithScale =>
      cellTextStyle?.apply(fontSizeFactor: scale);

  @override
  double computeCellWidth(
    GridColumn columnSF,
    DataGridRow rowSF,
    // Почему-то cellValue всегда null, приходится переопределять ниже
    Object? cellValue,
    TextStyle textStyle,
  ) {
    // TODO Ilya: method to get GridColumn's entity?
    final columnIndex = dataSource.columnsSF
        .indexOf(dataSource.getExtendedSFColumnByNotExtended(columnSF)!);
    final columnEntity = dataSource.columnsEntities[columnIndex];

    if (columnEntity.editable && columnEntity.type == ColumnType.bool) {
      return RowCellEditSwitch.width;
    }

    final cell = rowSF.getCells()[columnIndex];

    cellValue = tableCellFormatter.format(
      cell.value,
      columnEntity,
    );

    return super.computeCellWidth(
          columnSF,
          rowSF,
          cellValue,
          cellTextStyleWithScale ?? textStyle,
        ) +
        // TODO Ilya: 16 is padding duplicates (autoFitPadding)
        // TODO Ilya: 4 need to be added after flutter upgrade (don't know why)
        16 +
        4;
  }

  @override
  double computeHeaderCellWidth(
    GridColumn column,
    TextStyle style,
  ) =>
      super.computeHeaderCellWidth(
        column,
        headerTextStyleWithScale ?? style,
      ) +
      (column.allowFiltering
          ? (ColumnWidget.iconSize * scale +
              ColumnWidget.spaceBetweenIcons * scale)
          : 0) +
      (column.allowSorting ? ColumnWidget.iconSize * scale : 0) +
      // Дополнительное пространство, а то текст почему-то переносится
      20;
}
