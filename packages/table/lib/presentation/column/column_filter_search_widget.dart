import 'package:flutter/material.dart';

// Copy of the FilterSearchWidget
class ColumnFilterSearchWidget extends StatefulWidget {
  final Function(String) onChanged;
  final Function() cleared;
  final String? initialValue;

  const ColumnFilterSearchWidget({
    super.key,
    required this.onChanged,
    required this.cleared,
    this.initialValue,
  });

  @override
  State<ColumnFilterSearchWidget> createState() => _SearchWidgetState();
}

class _SearchWidgetState extends State<ColumnFilterSearchWidget> {
  final TextEditingController _controller = TextEditingController();

  @override
  void initState() {
    super.initState();

    if (widget.initialValue != null) {
      _controller.text = widget.initialValue!;
    }
  }

  @override
  Widget build(BuildContext context) {
    final theme = Theme.of(context);
    return Padding(
      padding: const EdgeInsets.only(bottom: 12),
      child: TextFormField(
        autofocus: true,
        controller: _controller,
        onChanged: widget.onChanged,
        decoration: InputDecoration(
          hintText: 'поиск',
          hintStyle: theme.textTheme.subtitle2,
          border: const OutlineInputBorder(),
          contentPadding: const EdgeInsets.fromLTRB(12, 12, 8, 12),
          suffixIcon: IconButton(
            iconSize: 20,
            splashRadius: 20,
            color: Theme.of(context).iconTheme.color,
            onPressed: () {
              _controller.clear();
              widget.cleared();
            },
            icon: const Icon(Icons.close),
          ),
        ),
      ),
    );
  }

  @override
  void dispose() {
    _controller.dispose();
    super.dispose();
  }
}
