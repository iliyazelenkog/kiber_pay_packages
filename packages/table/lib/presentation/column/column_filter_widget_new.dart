import 'dart:async';

import 'package:calendar_date_picker2/calendar_date_picker2.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:table/domain/entities/column_entity.dart';
import 'package:table/domain/filters/table_filters.dart';
import 'package:table/infrastructure/filters/table_filters_json_impl.dart';
import 'package:table/presentation/bloc/events/table_event.dart';
import 'package:table/presentation/extensions/di_on_build_context.dart';
import 'package:table/table.dart';

import '../../domain/filters/list_filter.dart';
import '../../infrastructure/filters/list_table_filter_json_impl.dart';
import '../app_popup_menu.dart';
import '../bloc/table_bloc.dart';
import 'column_filter_bool_widget.dart';
import 'column_filter_search_widget.dart';

enum _FilterType {
  bool,
  autocomplete,
  date,
}

class ColumnFilterWidgetNew extends StatefulWidget {
  final ColumnEntity columnEntity;
  final bool multiSelection;
  final Function()? onOpen;
  final Function()? onDispose;
  final double iconSize;

  const ColumnFilterWidgetNew({
    super.key,
    required this.columnEntity,
    this.onOpen,
    this.onDispose,
    this.multiSelection = true,
    this.iconSize = 18.0,
  });

  @override
  State<ColumnFilterWidgetNew> createState() => ColumnFilterWidgetNewState();
}

class ColumnFilterWidgetNewState extends State<ColumnFilterWidgetNew> {
  static final _Debounce _searchItemsDebounce =
      _Debounce(const Duration(milliseconds: 600));

  final GlobalKey<AppPopupMenuButtonState> _key = GlobalKey();
  late final _bloc = context.bloc<TableBloc>();
  // TODO Ilya: logic to cubit (1 cubit instance per widget)
  // late final _cubit = context.bloc<FilterWindowCubit>();

  var isChangedByUser = false;
  var isSearchLoading = false;
  var loading = false;
  FilterItemsType _initList = [];
  int? _listIndexToApplyDividerAfterSelected;
  FilterItemsType _list = [];
  FilterItemsType _buffSelected = [];
  FilterItemsType _selected = [];
  var _selectedDates = <DateTime>[];
  String filterInput = '';

  TableFilter? get _myFilter => _bloc.state.filter[widget.columnEntity.key];

  late bool? _filterBoolValue;

  double get _height => _filterType == _FilterType.bool ? 220 : 450;

  _FilterType get _filterType => widget.columnEntity.type == ColumnType.bool
      ? _FilterType.bool
      : widget.columnEntity.type == ColumnType.date
          ? _FilterType.date
          : _FilterType.autocomplete;

  bool get _isFilterModified {
    if (_filterType == _FilterType.bool) {
      final boolFilter = _myFilter as BoolTableFilter?;

      return boolFilter?.value != null;
    } else if (_filterType == _FilterType.autocomplete) {
      return _selected.isNotEmpty;
    } else if (_filterType == _FilterType.date) {
      print(_selectedDates.length);

      return _selectedDates.length == 2;
    }

    throw UnsupportedError('Unsupported filter type: $_filterType');
  }

  void _initStartValues() {
    setState(() {
      if (_myFilter is ListTableFilter) {
        _selected = (_myFilter as ListTableFilter).selectedItems;
      } else if (_myFilter is DateTableFilter) {
        _selectedDates = [
          (_myFilter as DateTableFilter).from,
          (_myFilter as DateTableFilter).to,
        ];
      }

      _filterBoolValue = _myFilter is BoolTableFilter
          ? (_myFilter as BoolTableFilter).value == null
          : null;
    });
  }

  void _changeFilter(ApplyFilterTableEvent event) {
    isChangedByUser = true;

    _bloc.add(event);
  }

  Future<FilterItemsType>? lastSearchItemsFuture;

  Future<FilterItemsType?> _searchItems(String? input) async {
    final filterItemsResolver = context.read<FilterItemsResolverCallback>();

    final future = filterItemsResolver(
      input: input,
      column: widget.columnEntity.key,
    );
    lastSearchItemsFuture = future;

    final items = await lastSearchItemsFuture;

    if (future.hashCode != lastSearchItemsFuture.hashCode) {
      return null;
    }

    return items;
  }

  void _showMenu() {
    _key.currentState?.showButtonMenu();
  }

  Future<void> _onTap() async {
    if (_filterType == _FilterType.date) {
      // TODO Ilya: don't depend on external calendar (just use some getter to receive date range)
      final selectedDate = await showCalendarDatePicker2Dialog(
        context: context,
        config: CalendarDatePicker2WithActionButtonsConfig(
          calendarType: CalendarDatePicker2Type.range,
          // cancelButton: Row(
          //   children: [
          //     TextButton(
          //       onPressed: () => Navigator.of(context).pop(),
          //       child: Text('Сбросить'),
          //     ),
          //     TextButton(
          //       onPressed: () => Navigator.of(context).pop(),
          //       child: Text('Отмена'),
          //     ),
          //   ],
          // ),
        ),
        dialogSize: const Size(325, 400),
        value: _selectedDates.length == 2
            ? [
                _selectedDates[0],
                _selectedDates[1],
                // DateTime.now().subtract(const Duration(days: 3)),
                // DateTime.now(),
              ]
            : [],
        borderRadius: BorderRadius.circular(15),
      );

      _changeFilter(ApplyFilterTableEvent(
        filterCurrentColumn: widget.columnEntity,
        filter: selectedDate?.length == 2
            ? DateTableFilterImpl(
                from: selectedDate!.first!,
                to: selectedDate.last!,
              )
            : null,
      ));

      setState(() {
        _selectedDates = selectedDate?.length == 2
            ? [selectedDate!.first!, selectedDate.last!]
            : [];
      });
    } else if (_filterType == _FilterType.bool) {
      _showMenu();
    } else if (_filterType == _FilterType.autocomplete) {
      final currentInput = filterInput;

      setState(() {
        loading = true;
      });

      final items = await _searchItems(currentInput);

      if (items == null) {
        return;
      }

      final listWithoutSelected =
          items.where((element) => !_selected.contains(element)).toList();
      final listWithSelectedAtStart = _selected + listWithoutSelected;
      final sortedList = listWithSelectedAtStart
        ..sort((a, b) {
          if (_selected.contains(a)) {
            return -1;
          } else if (_selected.contains(b)) {
            return 1;
          }

          return 0;
        });

      final listIndexToApplyDividerAfterSelected =
          sortedList.lastIndexWhere((e) => _selected.contains(e));

      setState(() {
        _listIndexToApplyDividerAfterSelected =
            listIndexToApplyDividerAfterSelected;
        _initList = sortedList;
        _list = _initList;
        loading = false;
      });

      WidgetsBinding.instance.addPostFrameCallback((_) {
        _showMenu();
      });
    }
  }

  @override
  initState() {
    super.initState();

    _initStartValues();
  }

  @override
  Widget build(BuildContext context) {
    final theme = Theme.of(context);

    return BlocListener<TableBloc, TableState>(
      listenWhen: (p, c) => p.filter != c.filter,
      listener: (context, state) {
        // TODO Ilya: maybe remove condition to allow setting filter from client at any time
        // If not modified by user manually
        if (!isChangedByUser) {
          // Apply data from cache/or from module's client
          _initStartValues();
        }
      },
      child: Row(
        children: [
          SizedBox.square(
            // Только iconSize у IconButton не ставит размер почему-то
            dimension: widget.iconSize,
            child: loading
                ? const CircularProgressIndicator(strokeWidth: 2)
                : IconButton(
                    onPressed: _onTap,
                    iconSize: widget.iconSize,
                    splashRadius: widget.iconSize - 6,
                    padding: EdgeInsets.zero,
                    icon: Icon(
                      Icons.filter_alt_outlined,
                      color: _isFilterModified ? theme.primaryColor : null,
                    ),
                  ),
          ),
          AppPopupMenuButton(
            key: _key,
            enabled: false,
            tooltip: 'Фильтр',
            color: theme.canvasColor,
            padding: EdgeInsets.zero,
            child: const SizedBox.shrink(),
            itemBuilder: (context) {
              _buffSelected = _selected;
              return [
                AppPopupMenuItem(
                  enabled: false,
                  onInit: () {
                    widget.onOpen?.call();
                    _bloc.add(ColumnFilterPopupOpenStatus(isOpen: true));
                  },
                  onDispose: () {
                    widget.onDispose?.call();
                    _bloc.add(ColumnFilterPopupOpenStatus(isOpen: false));
                  },
                  child: SizedBox(
                    height: _height,
                    width: 400,
                    // TODO Ilya: why StatefulBuilder if used top-level state?
                    child: StatefulBuilder(
                      builder: (context, state) {
                        return CustomScrollView(
                          slivers: [
                            SliverToBoxAdapter(
                              child: Row(
                                children: [
                                  Text(
                                    'Фильтр ${_selected.isNotEmpty ? '(выбрано ${_selected.length})' : ''}',
                                    style: theme.textTheme.labelLarge!,
                                  ),
                                  const Spacer(),
                                  IconButton(
                                    onPressed: Navigator.of(context).pop,
                                    icon: const Icon(Icons.close),
                                    splashRadius: 24,
                                  ),
                                ],
                              ),
                            ),
                            const SliverToBoxAdapter(
                              child: Divider(),
                            ),
                            if (_filterType == _FilterType.bool)
                              SliverToBoxAdapter(
                                child: ColumnFilterBoolWidget(
                                  boolFilter: _myFilter as BoolTableFilter?,
                                  onChange: (bool? val) {
                                    state(() {
                                      _filterBoolValue = val;
                                    });
                                  },
                                ),
                              ),
                            // TODO Ilya: refactor to separated widget
                            if (_filterType == _FilterType.autocomplete) ...[
                              SliverToBoxAdapter(
                                child: ColumnFilterSearchWidget(
                                  initialValue: filterInput,
                                  onChanged: (input) async {
                                    state(() {
                                      filterInput = input;
                                      isSearchLoading = true;
                                    });

                                    _searchItemsDebounce(
                                      () async {
                                        final items = await _searchItems(input);

                                        if (items == null) {
                                          return;
                                        }

                                        state(() {
                                          _list = items;
                                          isSearchLoading = false;
                                        });
                                      },
                                    );
                                  },
                                  cleared: () {
                                    state(() {
                                      _list = _initList;
                                    });
                                  },
                                ),
                              ),
                              SliverToBoxAdapter(
                                child: isSearchLoading
                                    ? const Center(
                                        child: SizedBox.square(
                                          dimension: 14,
                                          child: CircularProgressIndicator(),
                                        ),
                                      )
                                    : (_list.isEmpty
                                        ? Center(
                                            child: Text(
                                              'Ничего не найдено',
                                              style: theme.textTheme.bodySmall,
                                            ),
                                          )
                                        : const SizedBox.shrink()),
                              ),
                              SliverToBoxAdapter(
                                child: SizedBox(
                                  height: 280,
                                  child: ListView.builder(
                                    shrinkWrap: true,
                                    itemCount:
                                        isSearchLoading ? 0 : _list.length,
                                    itemBuilder:
                                        (BuildContext context, int index) {
                                      final item = _list[index];
                                      final text = item.text;

                                      return Column(
                                        children: [
                                          CheckboxListTile(
                                            value: _buffSelected.contains(item),
                                            title: Text(
                                              text.toString(),
                                              style: theme.textTheme.bodyMedium,
                                            ),
                                            contentPadding:
                                                const EdgeInsets.symmetric(
                                              horizontal: 6,
                                              vertical: 2,
                                            ),
                                            onChanged: (bool? value) {
                                              if (!widget.multiSelection) {
                                                state(() {
                                                  _buffSelected.clear();
                                                  _buffSelected.add(item);
                                                });
                                                return;
                                              }

                                              state(() {
                                                if (_buffSelected
                                                    .contains(item)) {
                                                  _buffSelected.remove(item);
                                                } else {
                                                  _buffSelected.add(item);
                                                }
                                              });
                                            },
                                          ),
                                          if (index ==
                                              _listIndexToApplyDividerAfterSelected)
                                            const Divider(),
                                        ],
                                      );
                                    },
                                  ),
                                ),
                              ),
                            ],
                            const SliverPadding(
                                padding: EdgeInsets.only(top: 8)),
                            SliverToBoxAdapter(
                              child: Row(
                                children: [
                                  Expanded(
                                    child: SizedBox(
                                      height: 40,
                                      child: ElevatedButton(
                                        style: ElevatedButton.styleFrom(
                                          foregroundColor: theme.primaryColor,
                                          backgroundColor: theme.canvasColor,
                                          elevation: 0,
                                        ),
                                        onPressed: () {
                                          if (_filterType ==
                                              _FilterType.autocomplete) {
                                            state(() {
                                              _selected = [];
                                              _buffSelected = [];
                                            });
                                          }

                                          _changeFilter(ApplyFilterTableEvent(
                                            filterCurrentColumn:
                                                widget.columnEntity,
                                            filter: _filterType ==
                                                    _FilterType.autocomplete
                                                ? ListTableFilterImpl(
                                                    selectedItems: [],
                                                  )
                                                : BoolTableFilterImpl(
                                                    value: null,
                                                  ),
                                          ));

                                          Navigator.of(context).pop();
                                        },
                                        child: Text(
                                          'Сбросить',
                                          style: theme.textTheme.labelLarge!,
                                        ),
                                      ),
                                    ),
                                  ),
                                  const SizedBox(
                                    width: 8,
                                  ),
                                  Expanded(
                                    child: SizedBox(
                                      height: 40,
                                      child: ElevatedButton(
                                        onPressed: () {
                                          if (_filterType ==
                                              _FilterType.autocomplete) {
                                            state(() {
                                              _selected = _buffSelected;
                                            });
                                          }

                                          _changeFilter(ApplyFilterTableEvent(
                                            filterCurrentColumn:
                                                widget.columnEntity,
                                            filter: _filterType ==
                                                    _FilterType.autocomplete
                                                ? ListTableFilterImpl(
                                                    selectedItems: _selected
                                                        .map((e) =>
                                                            ListTableFilterItemImpl(
                                                              value: e.value,
                                                              text: e.text,
                                                            ))
                                                        .toList(),
                                                  )
                                                : BoolTableFilterImpl(
                                                    value: _filterBoolValue,
                                                  ),
                                          ));

                                          Navigator.of(context).pop();
                                        },
                                        child: Text(
                                          'Применить',
                                        ),
                                      ),
                                    ),
                                  ),
                                ],
                              ),
                            )
                          ],
                        );
                      },
                    ),
                  ),
                )
              ];
            },
          ),
        ],
      ),
    );
  }
}

class _Debounce {
  Duration delay;
  Timer? _timer;

  _Debounce(
    this.delay,
  );

  call(void Function() callback) {
    _timer?.cancel();
    _timer = Timer(delay, callback);
  }

  dispose() {
    _timer?.cancel();
  }
}
