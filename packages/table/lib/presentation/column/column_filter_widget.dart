import 'package:flutter/material.dart';

import '../app_popup_menu.dart';
import 'column_filter_search_widget.dart';

// Copy of the HeaderFilterWidget
class ColumnFilterWidget extends StatefulWidget {
  const ColumnFilterWidget({
    super.key,
    required this.list,
    required this.selected,
    required this.onSelected,
    this.onOpen,
    this.onDispose,
    this.title = '',
    this.multiSelection = true,
    this.size,
  });

  final List<dynamic> list;
  final List<dynamic> selected;
  final Function(List<dynamic>) onSelected;
  final String title;
  final bool multiSelection;
  final Function()? onOpen;
  final Function()? onDispose;
  final double? size;

  @override
  State<ColumnFilterWidget> createState() => _ColumnFilterWidgetState();
}

class _ColumnFilterWidgetState extends State<ColumnFilterWidget> {
  var _selected = [];
  var _buffSelected = [];
  var _list = [];

  @override
  void initState() {
    super.initState();

    _list = widget.list;
    _selected = widget.selected;
  }

  @override
  void didUpdateWidget(covariant ColumnFilterWidget oldWidget) {
    super.didUpdateWidget(oldWidget);

    _list = widget.list;
    _selected = widget.selected;
  }

  @override
  Widget build(BuildContext context) {
    final theme = Theme.of(context);
    return AppPopupMenuButton(
      tooltip: 'Фильтр',
      color: theme.canvasColor,
      padding: EdgeInsets.zero,
      child: widget.title.isEmpty
          ? Icon(
              Icons.filter_alt_outlined,
              color: _selected.isNotEmpty ? theme.primaryColor : null,
              size: widget.size,
            )
          : Text(
              widget.title,
              style: theme.textTheme.labelLarge!.copyWith(
                  color: _selected.isNotEmpty ? theme.primaryColor : null),
            ),
      itemBuilder: (context) {
        _buffSelected = widget.selected;
        return [
          AppPopupMenuItem(
            enabled: false,
            onInit: widget.onOpen,
            onDispose: widget.onDispose,
            child: SizedBox(
              height: 457,
              width: 400,
              // TODO Ilya: why StatefulBuilder if used top-level state?
              child: StatefulBuilder(builder: (context, state) {
                return CustomScrollView(
                  slivers: [
                    SliverToBoxAdapter(
                      child: Row(
                        children: [
                          const Text('Фильтр'),
                          const Spacer(),
                          IconButton(
                            onPressed: Navigator.of(context).pop,
                            icon: const Icon(Icons.close),
                            splashRadius: 24,
                          ),
                        ],
                      ),
                    ),
                    const SliverToBoxAdapter(
                      child: Divider(),
                    ),
                    SliverToBoxAdapter(
                      child: ColumnFilterSearchWidget(
                        onChanged: (value) {
                          var filtered = widget.list;

                          if (widget.list[0] is List) {
                            filtered = filtered
                                .where((e) => e[1].contains(value))
                                .toList();
                          } else {
                            filtered = filtered
                                .where((e) => e.contains(value))
                                .toList();
                          }

                          state(() {
                            _list = filtered;
                          });
                        },
                        cleared: () {
                          state(() {
                            _list = widget.list;
                          });
                        },
                      ),
                    ),
                    SliverToBoxAdapter(
                      child: SizedBox(
                        height: 300,
                        child: ListView.builder(
                            shrinkWrap: true,
                            itemCount: _list.length,
                            itemBuilder: (BuildContext context, int index) {
                              final item = _list[index];

                              final selectItem = item is List ? item[0] : item;

                              final text = item is List ? item[1] : item;
                              return CheckboxListTile(
                                  value: _buffSelected.contains(selectItem),
                                  title: Text(
                                    text.toString(),
                                    style: theme.textTheme.bodyMedium,
                                  ),
                                  contentPadding: const EdgeInsets.symmetric(
                                    horizontal: 6,
                                    vertical: 2,
                                  ),
                                  onChanged: (bool? value) {
                                    if (!widget.multiSelection) {
                                      state(() {
                                        _buffSelected.clear();
                                        _buffSelected.add(selectItem);
                                      });
                                      return;
                                    }

                                    state(() {
                                      if (_buffSelected.contains(selectItem)) {
                                        _buffSelected.remove(selectItem);
                                      } else {
                                        _buffSelected.add(selectItem);
                                      }
                                    });
                                  });
                            }),
                      ),
                    ),
                    SliverToBoxAdapter(
                      child: Row(
                        children: [
                          Expanded(
                            child: SizedBox(
                              height: 40,
                              child: ElevatedButton(
                                style: ElevatedButton.styleFrom(
                                  foregroundColor: theme.primaryColor,
                                  backgroundColor: theme.canvasColor,
                                  elevation: 0,
                                ),
                                onPressed: () {
                                  setState(() {
                                    _selected = [];
                                    _buffSelected = [];
                                  });

                                  widget.onSelected([]);

                                  Navigator.of(context).pop();
                                },
                                child: Text(
                                  'Сбросить',
                                  style: theme.textTheme.labelLarge!,
                                ),
                              ),
                            ),
                          ),
                          const SizedBox(
                            width: 8,
                          ),
                          Expanded(
                            child: SizedBox(
                              height: 40,
                              child: ElevatedButton(
                                onPressed: () {
                                  setState(() {
                                    _selected = _buffSelected;
                                  });

                                  widget.onSelected(_selected);

                                  Navigator.of(context).pop();
                                },
                                child: Text(
                                  'Применить',
                                ),
                              ),
                            ),
                          ),
                        ],
                      ),
                    )
                  ],
                );
              }),
            ),
          )
        ];
      },
    );
  }
}
