import 'package:flutter/material.dart';
import 'package:table/domain/filters/table_filters.dart';

class ColumnFilterBoolWidget extends StatefulWidget {
  final BoolTableFilter? boolFilter;
  final void Function(bool? val) onChange;

  const ColumnFilterBoolWidget({
    super.key,
    required this.boolFilter,
    required this.onChange,
  });

  @override
  State<ColumnFilterBoolWidget> createState() => _ColumnFilterBoolWidgetState();
}

class _ColumnFilterBoolWidgetState extends State<ColumnFilterBoolWidget> {
  late bool checkboxVal = widget.boolFilter?.value == null;
  late bool trueFalseSwitchVal = widget.boolFilter?.value ?? false;

  @override
  Widget build(BuildContext context) {
    final theme = Theme.of(context);

    return SizedBox(
      height: 110,
      child: Column(
        children: [
          CheckboxListTile(
            title: Text(
              'Все значения (true и false)',
              style: theme.textTheme.bodyMedium,
            ),
            value: checkboxVal,
            onChanged: (value) {
              setState(() {
                checkboxVal = value!;
              });

              widget.onChange(value == false ? trueFalseSwitchVal : null);
            },
          ),
          const SizedBox(height: 10),
          if (checkboxVal)
            const SizedBox.shrink()
          else
            SwitchListTile.adaptive(
              title: Text(
                'false/true',
                style: theme.textTheme.bodyMedium,
              ),
              value: trueFalseSwitchVal,
              onChanged: (value) {
                setState(() {
                  trueFalseSwitchVal = value;
                });

                widget.onChange(trueFalseSwitchVal);
              },
            ),
        ],
      ),
    );
  }
}
