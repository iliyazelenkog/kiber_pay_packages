import 'package:flutter/widgets.dart';
import 'package:table/infrastructure/entities/column_entity_impl.dart';

import '../../domain/entities/column_entity.dart';
import '../../domain/entities/row_entity.dart';
import '../data_source/data_grid_cell_extended.dart';
import '../data_source/table_data_grid_source_base.dart';

class CustomColumnEntityImpl extends ColumnEntityImpl {
  final CustomColumnBuilder builder;

  CustomColumnEntityImpl({
    required super.key,
    required super.label,
    required this.builder,
    super.editable = false,
    super.filterable = false,
    super.sortable = false,
    super.width,
    super.autoWidth,
  }) : super(
          type: ColumnType.custom,
        );
}

typedef CustomColumnBuilder = Widget Function(
  BuildContext context,
  CustomColumnBuilderInfo info,
);

class CustomColumnBuilderInfo {
  final ColumnEntity column;
  final RowEntity row;
  final DataGridCellExtended cell;
  final TableDataGridSourceBase dataSource;

  CustomColumnBuilderInfo({
    required this.column,
    required this.row,
    required this.cell,
    required this.dataSource,
  });
}
