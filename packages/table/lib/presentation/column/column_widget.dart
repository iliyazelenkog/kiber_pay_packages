import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:table/presentation/bloc/table_bloc.dart';
import 'package:table/presentation/extensions/di_on_build_context.dart';

import '../../domain/column_sort_type.dart';
import '../../domain/entities/column_entity.dart';
import '../bloc/events/table_event.dart';
import 'column_filter_widget_new.dart';

class ColumnWidget extends StatelessWidget {
  final ColumnEntity columnEntity;

  const ColumnWidget({
    super.key,
    required this.columnEntity,
  });

  static const iconSize = 20.0;
  static const spaceBetweenIcons = 2.0;

  void _sort(BuildContext context) {
    final bloc = context.bloc<TableBloc>();

    bloc.add(SortTableEvent(
      column: columnEntity.key,
      sortType: _getSortType(bloc.state.sortBy[columnEntity.key]),
    ));
  }

  ColumnSortType? _getSortType(ColumnSortType? type) {
    switch (type) {
      case null:
        return ColumnSortType.ASC;
      case ColumnSortType.ASC:
        return ColumnSortType.DESC;
      case ColumnSortType.DESC:
        return null;
    }
  }

  @override
  Widget build(BuildContext context) {
    final theme = Theme.of(context);

    return BlocBuilder<TableBloc, TableState>(
      buildWhen: (p, c) => p.scale != c.scale || p.isLoading != c.isLoading,
      builder: (context, state) {
        final iconSize = ColumnWidget.iconSize * state.scale;

        return Stack(
          children: [
            if (columnEntity.editable && columnEntity.type != ColumnType.bool)
              Positioned(
                top: 0,
                right: 0,
                child: Tooltip(
                  message: 'Редактируемо',
                  child: Icon(Icons.edit, size: 14 * state.scale),
                ),
              ),
            Padding(
              key: ValueKey(columnEntity.key),
              padding: const EdgeInsets.symmetric(horizontal: 8),
              child: Row(
                children: [
                  Expanded(
                    child: Align(
                      alignment: Alignment.centerLeft,
                      child: Text(
                        columnEntity.label,
                        style: theme.textTheme.bodyLarge?.apply(
                          fontSizeFactor: state.scale,
                        ),
                      ),
                    ),
                  ),
                  if (columnEntity.filterable) ...[
                    BlocBuilder<TableBloc, TableState>(
                      buildWhen: (prev, curr) =>
                          prev.filter[columnEntity.key] !=
                          curr.filter[columnEntity.key],
                      builder: (context, state) => ColumnFilterWidgetNew(
                        // Хак чтобы обновлялось состояние виджета когда меняется filterBy и в целом состояние. А то, например, не было подсветки что выбран фильтр иногда
                        key: ValueKey(state.filter[columnEntity.key]),
                        columnEntity: columnEntity,
                        iconSize: iconSize,
                      ),
                    ),
                    SizedBox(
                        width: ColumnWidget.spaceBetweenIcons * state.scale),
                  ],
                  if (columnEntity.sortable)
                    BlocBuilder<TableBloc, TableState>(
                      buildWhen: (prev, curr) =>
                          prev.sortBy[columnEntity.key] !=
                          curr.sortBy[columnEntity.key],
                      builder: (context, state) {
                        final sortBy = state.sortBy[columnEntity.key];
                        final isSortBy = sortBy != null;
                        final isSortDesc = sortBy == ColumnSortType.DESC;

                        return SizedBox.square(
                          // Только iconSize у IconButton не ставит размер почему-то
                          dimension: iconSize,
                          child: IconButton(
                            iconSize: iconSize,
                            splashRadius: iconSize - 6,
                            onPressed: () => _sort(context),
                            padding: EdgeInsets.zero,
                            icon: isSortBy
                                ? Icon(
                                    isSortDesc
                                        ? Icons.arrow_downward_rounded
                                        : Icons.arrow_upward_rounded,
                                    color: theme.primaryColor,
                                  )
                                : const Icon(
                                    Icons.swap_vert_rounded,
                                  ),
                          ),
                        );
                      },
                    ),
                ],
              ),
            ),
          ],
        );
      },
    );
  }
}
