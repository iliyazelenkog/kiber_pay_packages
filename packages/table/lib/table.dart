import 'dart:async';

import 'package:flutter/cupertino.dart';
import 'package:injectable/injectable.dart';
import 'package:modules_basis/module.dart' as module;

import 'domain/entities/column_entity.dart';
import 'domain/filters/list_filter.dart';

typedef OnCopyCallback = void Function(BuildContext ctx, String copiedText);
// Метод который возвращает элементы для фильтра.
typedef FilterItemsResolverCallback = Future<FilterItemsType> Function({
  // По какой колонке идёт поиск
  required ColumnEntityKeyType column,
  // Что ввёл юзер в поле поиска
  String? input,
});

@InjectableInit.microPackage()
@lazySingleton
class TableModule extends module.Module<TableModuleResult> {
  @override
  Future<TableModuleResult> execute() async {
    return TableModuleResult();
  }
}

class TableModuleResult extends module.ModuleResult {}
