import 'package:json_annotation/json_annotation.dart';

import '../../domain/entities/column_entity.dart';

part 'column_entity_impl.g.dart';

@JsonSerializable(createToJson: false)
class ColumnEntityImpl extends ColumnEntity {
  @override
  @JsonEnum()
  ColumnType get type;

  ColumnEntityImpl({
    required super.key,
    required super.type,
    required super.label,
    required super.editable,
    super.filterable,
    super.sortable,
    super.width,
    super.autoWidth,
  });

  factory ColumnEntityImpl.fromJson(Map<String, dynamic> json) =>
      _$ColumnEntityImplFromJson(json);
}
