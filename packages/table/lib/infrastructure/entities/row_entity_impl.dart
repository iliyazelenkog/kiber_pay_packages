import 'dart:ui';

import 'package:json_annotation/json_annotation.dart';

import '../../domain/entities/column_entity.dart';
import '../../domain/entities/row_entity.dart';

@JsonSerializable(createToJson: false)
class RowEntityImpl extends RowEntity {
  @override
  Map<ColumnEntityKeyType, RowEntityFieldImpl> fields;

  RowEntityImpl({
    required super.id,
    required this.fields,
  }) : super(
          fields: fields,
        );

  factory RowEntityImpl.fromJson(Map<String, dynamic> json) {
    assert(
      json['id']?['data'] != null,
      '"id" is required because it\'s used for editing',
    );

    final fields = Map.fromEntries(
      json.entries.map((e) {
        return MapEntry(
          e.key,
          RowEntityFieldImpl.fromJson(e.value),
        );
      }),
    );

    return RowEntityImpl(
      // TODO Ilya: include 'id' in the field only if it's in the columns
      id: json['id']?['data'],
      fields: fields,
    );
  }
}

class RowEntityFieldImpl extends RowEntityField {
  RowEntityFieldImpl({
    required super.data,
    required super.lightThemeColor,
    required super.darkThemeColor,
  });

  factory RowEntityFieldImpl.fromJson(Map<String, dynamic> json) {
    return RowEntityFieldImpl(
      data: _dataFromJSON(json['data']),
      darkThemeColor: _parseColor(json['color']?['black']),
      lightThemeColor: _parseColor(json['color']?['white']),
    );
  }

  // TODO Ilya: parse by column's type
  static dynamic _dataFromJSON(dynamic data) {
    // Parse date
    if (data is String && data.contains('T')) {
      final date = DateTime.tryParse(data);

      return date ?? data;
    }

    return data;
  }

  // From int like this https://convertingcolors.com/decimal-color-6942894.html
  static Color? _parseColor(int? colorInt) {
    Color getColorFromHex(String hexColor) {
      hexColor = hexColor.replaceAll('#', '');

      if (hexColor.length == 6) {
        hexColor = 'FF$hexColor';
      }

      return Color(int.parse('0x$hexColor'));
    }

    return colorInt == null
        ? null
        : getColorFromHex(colorInt.toRadixString(16));
  }
}
