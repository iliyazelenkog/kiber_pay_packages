// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'column_entity_impl.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

ColumnEntityImpl _$ColumnEntityImplFromJson(Map<String, dynamic> json) =>
    ColumnEntityImpl(
      key: json['key'] as String,
      type: $enumDecode(_$ColumnTypeEnumMap, json['type']),
      label: json['label'] as String,
      editable: json['editable'] as bool,
      filterable: json['filterable'] as bool? ?? false,
      sortable: json['sortable'] as bool? ?? true,
      width: json['width'],
      autoWidth: json['autoWidth'] as bool? ?? false,
    );

const _$ColumnTypeEnumMap = {
  ColumnType.bool: 'bool',
  ColumnType.string: 'string',
  ColumnType.num: 'num',
  ColumnType.float: 'float',
  ColumnType.date: 'date',
  ColumnType.money: 'money',
  ColumnType.checkbox: 'checkbox',
  ColumnType.custom: 'custom',
};
