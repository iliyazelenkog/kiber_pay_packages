// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'table_data_response_impl.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

TableDataResponseImpl _$TableDataResponseImplFromJson(
        Map<String, dynamic> json) =>
    TableDataResponseImpl(
      columns: (json['columns'] as List<dynamic>)
          .map((e) => ColumnEntityImpl.fromJson(e as Map<String, dynamic>))
          .toList(),
      rows: (json['rows'] as List<dynamic>)
          .map((e) => RowEntityImpl.fromJson(e as Map<String, dynamic>))
          .toList(),
      frozenColumnsCount: json['frozen_columns_count'] as int,
      highlightFooter: json['footer'] as bool,
      page: json['page'] as int,
      totalPages: json['totalPages'] as int,
    );
