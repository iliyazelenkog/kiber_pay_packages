import 'package:json_annotation/json_annotation.dart';
import 'package:table/infrastructure/entities/row_entity_impl.dart';

import '../../../domain/repositories/dto/table_data_response.dart';
import '../../entities/column_entity_impl.dart';

part 'table_data_response_impl.g.dart';

@JsonSerializable(createToJson: false)
class TableDataResponseImpl extends TableDataResponse {
  @override
  final List<ColumnEntityImpl> columns;

  @override
  List<RowEntityImpl> rows;

  @override
  @JsonKey(name: 'frozen_columns_count')
  final int frozenColumnsCount;

  @override
  @JsonKey(name: 'footer')
  final highlightFooter;

  TableDataResponseImpl({
    required this.columns,
    required this.rows,
    required this.frozenColumnsCount,
    required this.highlightFooter,
    required super.page,
    required super.totalPages,
  }) : super(
          columns: columns,
          rows: rows,
          frozenColumnsCount: frozenColumnsCount,
          highlightFooter: highlightFooter,
        );

  factory TableDataResponseImpl.fromJson(Map<String, dynamic> json) {
    json = _fixBackend(json);

    return _$TableDataResponseImplFromJson(json);
  }

  // Иногда columns c бэка приходит больше чем полей в каждом rows из-за чего в таблице была ошибка не соответсвия
  static Map<String, dynamic> _fixBackend(Map<String, dynamic> json) {
    final rowsJSON = (json['rows'] as List<dynamic>);
    final rowsKeys = rowsJSON.firstOrNull?.keys;

    // Test for color
    // rowsJSON.firstOrNull!['id']['color'] = {
    //   // 1777381119
    //   // 6942894
    //   'black': 6942894,
    //   'white': 6942894,
    // };
    // Test for missed keys
    // (json['columns'] as List<dynamic>).add({
    //   'key': 'test',
    //   'type': 'string',
    //   'label': 'label',
    //   'editable': false,
    //   'filterable': false,
    // });
    // Test for custom column options
    final columns = json['columns'] as List<dynamic>;
    // columns[0]['type'] = 'money';
    // columns[0]['label'] = 'Дата';
    // columns[0]['autoWidth'] = true;
    // columns[0]['width'] = null;

    // json['columns'].forEach((column) {
    //   column['autoWidth'] = true;
    //   column['width'] = null;
    // });

    // Бывало приходило два column с одинаковым key (id)
    json['columns'] = columns.unique((e) => e['key']);

    if (rowsKeys != null) {
      final columnsKeys = columns.map((e) => e['key']).toList();
      final missedKeys =
          columnsKeys.where((e) => !rowsKeys!.contains(e)).toList();

      for (var key in missedKeys) {
        for (var row in rowsJSON) {
          row[key] = '';
        }
      }
    }

    return json;
  }
}

extension _Unique<E, Id> on List<E> {
  List<E> unique([Id Function(E element)? id, bool inplace = true]) {
    final ids = <dynamic>{};
    var list = inplace ? this : List<E>.from(this);
    list.retainWhere((x) => ids.add(id != null ? id(x) : x as Id));
    return list;
  }
}
