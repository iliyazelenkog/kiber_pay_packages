// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'table_filters_json_impl.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

BoolTableFilterImpl _$BoolTableFilterImplFromJson(Map<String, dynamic> json) =>
    BoolTableFilterImpl(
      value: json['value'] as bool?,
    );

Map<String, dynamic> _$BoolTableFilterImplToJson(
        BoolTableFilterImpl instance) =>
    <String, dynamic>{
      'value': instance.value,
    };

DateTableFilterImpl _$DateTableFilterImplFromJson(Map<String, dynamic> json) =>
    DateTableFilterImpl(
      from: DateTime.parse(json['from'] as String),
      to: DateTime.parse(json['to'] as String),
    );

Map<String, dynamic> _$DateTableFilterImplToJson(
        DateTableFilterImpl instance) =>
    <String, dynamic>{
      'from': instance.from.toIso8601String(),
      'to': instance.to.toIso8601String(),
    };
