import 'package:json_annotation/json_annotation.dart';
import 'package:table/infrastructure/filters/table_filters_json_impl.dart';

import '../../domain/filters/list_filter.dart';

part 'list_table_filter_json_impl.g.dart';

@JsonSerializable()
class ListTableFilterItemImpl extends ListTableFilterItem {
  @JsonKey(name: 'id')
  @override
  final dynamic value;

  @JsonKey(name: 'display')
  @override
  final String text;

  ListTableFilterItemImpl({
    required this.value,
    required this.text,
  }) : super(value: value, text: text);

  Map<String, dynamic> toJson() => _$ListTableFilterItemImplToJson(this);

  factory ListTableFilterItemImpl.fromJson(Map<String, dynamic> json) =>
      _$ListTableFilterItemImplFromJson(json);
}

@JsonSerializable()
class ListTableFilterImpl extends ListTableFilter
    implements TableFilterJSONImpl {
  @override
  final List<ListTableFilterItemImpl> selectedItems;

  ListTableFilterImpl({
    required this.selectedItems,
  }) : super(selectedItems: selectedItems);

  factory ListTableFilterImpl.fromJson(Map<String, dynamic> json) =>
      _$ListTableFilterImplFromJson(json);

  @override
  String get type => 'list';

  @override
  Map<String, dynamic> toJson() => _$ListTableFilterImplToJson(this);

  @override
  List<dynamic> toJsonBackend() => selectedItems.map((e) => e.value).toList();
}
