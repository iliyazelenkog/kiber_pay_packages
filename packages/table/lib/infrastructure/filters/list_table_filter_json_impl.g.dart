// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'list_table_filter_json_impl.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

ListTableFilterItemImpl _$ListTableFilterItemImplFromJson(
        Map<String, dynamic> json) =>
    ListTableFilterItemImpl(
      value: json['id'],
      text: json['display'] as String,
    );

Map<String, dynamic> _$ListTableFilterItemImplToJson(
        ListTableFilterItemImpl instance) =>
    <String, dynamic>{
      'id': instance.value,
      'display': instance.text,
    };

ListTableFilterImpl _$ListTableFilterImplFromJson(Map<String, dynamic> json) =>
    ListTableFilterImpl(
      selectedItems: (json['selectedItems'] as List<dynamic>)
          .map((e) =>
              ListTableFilterItemImpl.fromJson(e as Map<String, dynamic>))
          .toList(),
    );

Map<String, dynamic> _$ListTableFilterImplToJson(
        ListTableFilterImpl instance) =>
    <String, dynamic>{
      'selectedItems': instance.selectedItems,
    };
