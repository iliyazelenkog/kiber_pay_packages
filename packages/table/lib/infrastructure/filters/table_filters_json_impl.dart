import 'package:json_annotation/json_annotation.dart';

import '../../domain/filters/table_filters.dart';

part 'table_filters_json_impl.g.dart';

abstract class TableFilterJSONImpl extends TableFilter {
  String get type;

  Map<String, dynamic> toJson();

  dynamic toJsonBackend();

  factory TableFilterJSONImpl.fromJson(Map<String, dynamic> json) {
    throw UnimplementedError();
  }
}

@JsonSerializable()
class BoolTableFilterImpl extends BoolTableFilter
    implements TableFilterJSONImpl {
  BoolTableFilterImpl({
    required super.value,
  });

  factory BoolTableFilterImpl.fromJson(Map<String, dynamic> json) =>
      _$BoolTableFilterImplFromJson(json);

  @override
  String get type => 'bool';

  @override
  Map<String, dynamic> toJson() => _$BoolTableFilterImplToJson(this);

  @override
  bool? toJsonBackend() => value;
}

@JsonSerializable(includeIfNull: false)
class DateTableFilterImpl extends DateTableFilter
    implements TableFilterJSONImpl {
  DateTableFilterImpl({
    required super.from,
    required super.to,
  });

  factory DateTableFilterImpl.fromJson(Map<String, dynamic> json) =>
      _$DateTableFilterImplFromJson(json);

  @override
  String get type => 'date';

  @override
  Map<String, dynamic> toJson() => toJsonBackend();

  @override
  Map<String, dynamic> toJsonBackend() => {
        'from': from.toString().substring(0, 10),
        'to': to.toString().substring(0, 10),
      };
}
