import '../../domain/filters/list_filter.dart';
import '../../domain/repositories/dynamic_table_read_repo.dart';
import '../filters/list_table_filter_json_impl.dart';
import '../filters/table_filters_json_impl.dart';

abstract class FilterByMapToJsonMapper {
  static TableFilterJSONImpl _parseNamedFilter(Map<String, dynamic> json) {
    final data = json['data'];

    switch (json['type']) {
      case 'list':
        return ListTableFilterImpl.fromJson(data);
      case 'bool':
        return BoolTableFilterImpl.fromJson(data);
      case 'date':
        return DateTableFilterImpl.fromJson(data);
      default:
        throw UnimplementedError('Unknown filter type: ${json['type']}');
    }
  }

  static DynamicTableFilterByType<TableFilterJSONImpl> fromJson(
      DynamicTableFilterByTypeRequest json) {
    return json.entries.fold<DynamicTableFilterByType<TableFilterJSONImpl>>(
      {},
      (prev, element) => prev
        ..addAll({
          element.key: _parseNamedFilter(element.value),
        }),
    );
  }

  static DynamicTableFilterByTypeRequest toJson(
    DynamicTableFilterByType<TableFilterJSONImpl> sortByType, {
    required isForBackend,
  }) {
    return sortByType.entries
        .where((element) => element.value != null)
        .fold<DynamicTableFilterByTypeRequest>(
      {},
      (prev, element) {
        final elVal = element.value!;
        late final dynamic json;

        if (elVal is ListTableFilter &&
            (elVal as ListTableFilter).selectedItems.isEmpty) {
          return prev;
        }

        if (isForBackend) {
          json = elVal.toJsonBackend();
        } else {
          json = {
            'type': elVal.type,
            'data': elVal.toJson(),
          };
        }

        if (json == null) {
          return prev;
        }

        return prev
          ..addAll({
            element.key: json,
          });
      },
    );
  }
}
