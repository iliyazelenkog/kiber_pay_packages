import '../../domain/column_sort_type.dart';
import '../../domain/entities/column_entity.dart';
import '../../domain/repositories/dynamic_table_read_repo.dart';

abstract class SortMapToJsonMapper {
  static Map<ColumnEntityKeyType, String> toJson(
    DynamicTableSortByType sortByType,
  ) =>
      sortByType.entries.fold<Map<ColumnEntityKeyType, String>>(
        {},
        (prev, element) {
          final nameJSON = element.value?.name;

          if (nameJSON == null) {
            return prev;
          }

          return prev..addAll({element.key: nameJSON});
        },
      );

  static DynamicTableSortByType fromJson(
    Map<ColumnEntityKeyType, dynamic> json,
  ) =>
      json.entries.fold<DynamicTableSortByType>(
        {},
        (prev, element) => prev
          ..addAll({
            element.key: element.value == ColumnSortType.ASC.name
                ? ColumnSortType.ASC
                : ColumnSortType.DESC
          }),
      );
}
