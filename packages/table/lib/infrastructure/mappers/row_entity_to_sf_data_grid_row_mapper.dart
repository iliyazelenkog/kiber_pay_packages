import '../../domain/entities/column_entity.dart';
import '../../domain/entities/row_entity.dart';
import '../../presentation/data_source/data_grid_cell_extended.dart';
import '../../presentation/data_source/data_grid_row_extended.dart';

abstract class RowEntityToSFDataGridRowMapper {
  static List<DataGridRowExtended> map(
    List<RowEntity> rows,
    List<ColumnEntity> columns,
  ) =>
      rows.map(
        (row) {
          final columnsKeys = columns.map((e) => e.key).toList();
          final sortedRows = row.visibleFields.entries.toList()
            ..sort(
              (a, b) => columnsKeys.indexOf(a.key).compareTo(
                    columnsKeys.indexOf(b.key),
                  ),
            );

          return DataGridRowExtended(
            cells: sortedRows.map(
              (field) {
                final fieldInfo = field.value;
                final fieldKey = field.key;
                final column = columns.firstWhere((col) => col.key == fieldKey);

                return DataGridCellExtended(
                  // TODO Ilya: column.key?
                  columnName: column.label,
                  value: fieldInfo.data,
                  lightThemeColor: fieldInfo.lightThemeColor,
                  darkThemeColor: fieldInfo.darkThemeColor,
                );
              },
            ).toList(),
          );
        },
      ).toList();
}
