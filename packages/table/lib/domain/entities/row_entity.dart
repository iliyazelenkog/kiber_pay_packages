import 'dart:ui';

import 'package:modules_basis/domain/entity.dart';

import 'column_entity.dart';

typedef DynamicTableRowFields = Map<ColumnEntityKeyType, RowEntityField>;
typedef RowIdType = dynamic;

class RowEntity extends Entity<RowIdType> {
  covariant DynamicTableRowFields fields;

  DynamicTableRowFields get visibleFields => Map.fromEntries(
      fields.entries.where((element) => element.value.isVisible));

  void hideFields(Set<ColumnEntityKeyType> cols) {
    fields.forEach((key, value) {
      fields[key]?.isVisible = !cols.contains(key);
    });
  }

  RowEntity({
    required id,
    required this.fields,
  }) : super(id);
}

class RowEntityField {
  dynamic data;
  final Color? lightThemeColor;
  final Color? darkThemeColor;
  bool isVisible;

  RowEntityField({
    required this.data,
    required this.lightThemeColor,
    required this.darkThemeColor,
    this.isVisible = true,
  });
}
