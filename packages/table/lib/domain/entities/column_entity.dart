import 'package:equatable/equatable.dart';
import 'package:modules_basis/domain/entity.dart';

typedef ColumnEntityKeyType = String;

class ColumnEntity extends Entity<ColumnEntityKeyType> with EquatableMixin {
  final ColumnEntityKeyType key;
  final ColumnType type;
  final String label;
  final bool editable, filterable, sortable, visible, autoWidth;
  int? _width;

  ColumnEntity({
    required this.key,
    required this.type,
    required this.label,
    // TODO Ilya: advanced edit
    // I think we need just add new type to ColumnType: list
    // editableOptions: {type: 'list', items: ['a', 'b',]} to edit as list. editableOptions is default to type: 'string'
    required this.editable,
    this.filterable = false,
    this.sortable = true,
    this.visible = true,
    this.autoWidth = false,
    width,
  }) : super(key) {
    _width = autoWidth ? null : width;
  }

  int? get width => _width;

  void resizeWidth(int val) {
    _width = val;
  }

  @override
  List<Object?> get props => [
        key,
        type,
        label,
        editable,
        filterable,
        sortable,
        visible,
        autoWidth,
        width,
      ];
}

enum ColumnType {
  bool,
  string,
  num,
  float,
  date,
  money,
  // TODO Ilya: the "custom" type? End render manually (pass widget), not hardcoded in the module
  checkbox,
  custom,
}
