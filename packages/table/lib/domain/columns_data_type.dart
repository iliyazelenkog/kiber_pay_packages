import 'entities/column_entity.dart';

// Мапа с данными которые принадлежат колонкам
typedef ColumnsDataType<T> = Map<ColumnEntityKeyType, T>;
