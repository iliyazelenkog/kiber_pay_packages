import 'package:equatable/equatable.dart';

import '../../filters/list_filter.dart';

class TableFilterResponse extends Equatable {
  final FilterItemsType items;

  const TableFilterResponse({
    required this.items,
  });

  @override
  List<Object?> get props => [items];
}
