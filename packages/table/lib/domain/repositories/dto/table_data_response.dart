import 'package:equatable/equatable.dart';
import 'package:table/domain/entities/column_entity.dart';
import 'package:table/domain/entities/row_entity.dart';

class TableDataResponse extends Equatable {
  List<ColumnEntity> columns;
  covariant List<RowEntity> rows;
  final int page;
  final int totalPages;
  final int frozenColumnsCount;
  final bool highlightFooter;

  TableDataResponse({
    required this.columns,
    required this.rows,
    required this.page,
    required this.totalPages,
    this.frozenColumnsCount = 0,
    this.highlightFooter = false,
  });

  @override
  List<Object?> get props => [
        columns,
        rows,
        page,
        totalPages,
        frozenColumnsCount,
        highlightFooter,
      ];
}
