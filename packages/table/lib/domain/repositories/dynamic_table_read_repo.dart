import 'package:table/domain/filters/table_filters.dart';

import '../column_sort_type.dart';
import '../columns_data_type.dart';
import '../entities/column_entity.dart';
import 'dto/table_data_response.dart';
import 'dto/table_filter_response.dart';

typedef DynamicTableSortByType = ColumnsDataType<ColumnSortType?>;
typedef DynamicTableFilterByType<T extends TableFilter> = ColumnsDataType<T?>;
typedef DynamicTableFilterByTypeRequest = ColumnsDataType<dynamic>;

abstract class DynamicTableReadRepo {
  Future<TableDataResponse> getTableData({
    required String tableId,
    required bool fullVariant,
    required int page,
    int? perPage,
    DynamicTableSortByType? sortBy,
    covariant DynamicTableFilterByTypeRequest? filterBy,
  });

  Future<TableFilterResponse> getTableFilter({
    required String tableId,
    required ColumnEntityKeyType column,
    required String? input,
    required bool fullVariant,
  });
}
