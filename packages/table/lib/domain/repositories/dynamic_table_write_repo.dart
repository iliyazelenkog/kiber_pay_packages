import '../entities/column_entity.dart';
import '../entities/row_entity.dart';

abstract class DynamicTableWriteRepo {
  Future<void> edit({
    required String tableId,
    required ColumnEntityKeyType column,
    required dynamic input,
    required RowIdType rowId,
    required bool fullVariant,
  });
}
