import 'package:equatable/equatable.dart';
import 'package:equatable/src/equatable_utils.dart';
import 'package:flutter/foundation.dart';

import 'table_filters.dart';

typedef FilterItemsType = List<ListTableFilterItem>;

class ListTableFilterItem extends Equatable {
  final dynamic value;
  final String text;

  const ListTableFilterItem({
    required this.value,
    required this.text,
  });

  @override
  List<Object?> get props => [value, text];
}

class ListTableFilter extends TableFilter with EquatableMixin {
  final FilterItemsType selectedItems;

  ListTableFilter({
    required this.selectedItems,
  });

  @override
  List<Object?> get props => [
        selectedItems,
        selectedItems.length,
      ];

  @override
  bool operator ==(Object other) =>
      // TODO Ilya: why I need check listEquals manually?
      (other is ListTableFilter
              ? listEquals(selectedItems, other.selectedItems)
              : true) &&
          identical(this, other) ||
      other is Equatable &&
          runtimeType == other.runtimeType &&
          equals(props, other.props);
}
