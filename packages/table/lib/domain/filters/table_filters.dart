import 'package:equatable/equatable.dart';

abstract class TableFilter {}

class BoolTableFilter extends TableFilter with EquatableMixin {
  final bool? value;

  BoolTableFilter({
    required this.value,
  });

  @override
  List<Object?> get props => [
        value,
      ];
}

class DateTableFilter extends TableFilter with EquatableMixin {
  final DateTime from;
  final DateTime to;

  DateTableFilter({
    required this.from,
    required this.to,
  });

  @override
  List<Object?> get props => [
        from,
        to,
      ];
}
