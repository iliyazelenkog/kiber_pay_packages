import 'package:table/domain/entities/column_entity.dart';

class EditRowColumnNotFound implements Exception {
  final ColumnEntityKeyType columnKey;

  EditRowColumnNotFound(this.columnKey);

  @override
  String toString() => 'Не удалось найти колонку $columnKey.';
}
