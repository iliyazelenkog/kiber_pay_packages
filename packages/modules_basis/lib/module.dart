import 'dart:async';

import 'module_invoker.dart';

abstract class ModuleResult {}

abstract class Module<R extends ModuleResult> {
  // final providers = <Provider>{};
  late final ModuleInvoker moduleInvoker;
  late final R executeResult;

  FutureOr<R> execute();

  void prepare({
    required ModuleInvoker moduleInvoker,
  }) {
    this.moduleInvoker = moduleInvoker;
  }

  // TODO Ilya: try to return Module<R>
  Future<Module> waitReady() async {
    late Module? alreadyInitFeature;

    try {
      // TODO Ilya: firstWhereOrNull after updating the version
      alreadyInitFeature =
          moduleInvoker.initializedModules.firstWhere((e) => e == this);
    } on StateError {
      alreadyInitFeature = null;
    }

    if (alreadyInitFeature != null) {
      return alreadyInitFeature;
    }

    // Это что-то не работает "alreadyInitFeature ?? ..."
    return moduleInvoker.streamInitializing.stream.firstWhere((e) => e == this);
  }
}
