import 'table_id_type.dart';

// Какие-либо данные таблицы в виде мапы
typedef TablesDataType<T> = Map<TableIdType, T>;
