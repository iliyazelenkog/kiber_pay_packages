import 'package:table/domain/columns_data_type.dart';

typedef ColumnsSizesType = ColumnsDataType<int>;
