import 'table_armor_exception.dart';

class TableArmorFetchException extends TableArmorException {
  TableArmorFetchException(super.message);

  @override
  String toString() => 'Не удалось получить данные.\n$message';
}
