abstract class TableArmorException implements Exception {
  final String message;

  TableArmorException(this.message);

  @override
  String toString() => message;
}
