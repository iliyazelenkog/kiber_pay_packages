//@GeneratedMicroModule;TableArmorPackageModule;package:table_armor/table_armor_module.module.dart
// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: unnecessary_lambdas
// ignore_for_file: lines_longer_than_80_chars
// coverage:ignore-file

// ignore_for_file: no_leading_underscores_for_library_prefixes
import 'dart:async' as _i2;

import 'package:cache_data_storage/domain/cache_data_storage.dart' as _i8;
import 'package:injectable/injectable.dart' as _i1;
import 'package:table/table.module.dart' as _i3;
import 'package:table_armor/application/use_cases/edit_table_use_case.dart'
    as _i7;
import 'package:table_armor/application/use_cases/get_table_data_use_case.dart'
    as _i5;
import 'package:table_armor/application/use_cases/get_table_filter_use_case.dart'
    as _i6;
import 'package:table_armor/presentation/bloc/table_armor_cubit.dart' as _i4;
import 'package:table_armor/table_armor_module.dart' as _i9;

class TableArmorPackageModule extends _i1.MicroPackageModule {
// initializes the registration of main-scope dependencies inside of GetIt
  @override
  _i2.FutureOr<void> init(_i1.GetItHelper gh) async {
    await _i3.TablePackageModule().init(gh);
    gh.factoryParam<_i4.TableArmorCubit, String, dynamic>((
      tableId,
      _,
    ) =>
        _i4.TableArmorCubit(
          tableId: tableId,
          getTableDataUseCase: gh<_i5.GetTableDataUseCase>(),
          getTableFilterUseCase: gh<_i6.GetTableFilterUseCase>(),
          editTableUseCase: gh<_i7.EditTableUseCase>(),
          cache: gh<_i8.CacheDataStorage>(),
          initState: gh<_i4.TableArmorState>(),
        ));
    gh.lazySingleton<_i9.TableArmorModule>(() => _i9.TableArmorModule());
  }
}
