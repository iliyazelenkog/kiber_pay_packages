import 'dart:async';

import 'package:flutter/material.dart';
import 'package:table_armor/presentation/toolbar/table_toolbar.dart';

class PaginationWidget extends StatefulWidget {
  final int totalPages;
  final int page;
  final FutureOr<void> Function(int) pressed;
  final int rowsPerPage;
  final ValueChanged<int?>? onRowsPerPageSelected;
  final List<int> availableRowsPerPage;

  PaginationWidget({
    super.key,
    required this.totalPages,
    required this.page,
    required this.pressed,
    rowsPerPage,
    this.onRowsPerPageSelected,
    this.availableRowsPerPage = defaultRowsPerPage,
  })  : rowsPerPage = rowsPerPage ??
            (availableRowsPerPage.isNotEmpty
                ? availableRowsPerPage.first
                : null),
        assert(
          rowsPerPage == null || availableRowsPerPage.contains(rowsPerPage),
          'The specified rowsPerPage is not in the availableRowsPerPage list.',
        );

  static const defaultRowsPerPage = [300, 200, 100, 50];

  @override
  State<PaginationWidget> createState() => _PaginationWidgetState();
}

class _PaginationWidgetState extends State<PaginationWidget> {
  late int page = widget.page;
  late int rowsPerPage = widget.rowsPerPage;

  @override
  Widget build(BuildContext context) {
    if (widget.totalPages <= 1) return const SizedBox.shrink();

    const splashRadius = TableToolbar.iconSize;

    return Row(
      children: [
        IconButton(
          onPressed: page > 1 ? () => setPage(1) : null,
          icon: const Icon(Icons.keyboard_double_arrow_left),
          splashRadius: splashRadius,
        ),
        IconButton(
          onPressed: page > 1 ? () => setPage(page - 1) : null,
          icon: const Icon(Icons.keyboard_arrow_left),
          splashRadius: splashRadius,
        ),
        PageButtonList(
          currentPage: page,
          totalPages: widget.totalPages,
          onPageSelected: setPage,
        ),
        IconButton(
          onPressed: page < widget.totalPages ? () => setPage(page + 1) : null,
          icon: const Icon(Icons.keyboard_arrow_right),
          splashRadius: splashRadius,
        ),
        IconButton(
          onPressed: page < widget.totalPages
              ? () => setPage(widget.totalPages)
              : null,
          icon: const Icon(Icons.keyboard_double_arrow_right),
          splashRadius: splashRadius,
        ),
        const SizedBox(width: 10),
        DropdownButton<int>(
          focusColor: Colors.transparent,
          padding: const EdgeInsets.symmetric(horizontal: 10),
          value: rowsPerPage,
          items: widget.availableRowsPerPage
              .map((int value) => DropdownMenuItem<int>(
                    value: value,
                    child: Text(value.toString()),
                  ))
              .toList(),
          onChanged: (int? newValue) {
            if (newValue != null) {
              setState(() {
                rowsPerPage = newValue;
              });

              widget.onRowsPerPageSelected?.call(newValue);
            }
          },
        ),
      ],
    );
  }

  void setPage(int newPage) {
    if (newPage < 1 || newPage > widget.totalPages) return;
    setState(() => page = newPage);
    widget.pressed(newPage);
  }
}

class PageButtonList extends StatelessWidget {
  final int currentPage;
  final int totalPages;
  final ValueChanged<int> onPageSelected;

  const PageButtonList({
    super.key,
    required this.currentPage,
    required this.totalPages,
    required this.onPageSelected,
  });

  @override
  Widget build(BuildContext context) {
    int startPage = currentPage - 1;
    if (startPage < 1) startPage = 1;
    int endPage = startPage + 2;
    if (endPage > totalPages) {
      endPage = totalPages;
      startPage = endPage - 2;
      if (startPage < 1) startPage = 1;
    }

    final theme = Theme.of(context);

    return Row(
      children: List<Widget>.generate(
        endPage - startPage + 1,
        (index) {
          int pageNumber = startPage + index;
          return SizedBox(
            width: 35,
            height: 40,
            child: TextButton(
              onPressed: () => onPageSelected(pageNumber),
              style: ElevatedButton.styleFrom(
                backgroundColor:
                    pageNumber == currentPage ? theme.primaryColor : null,
              ),
              child: Text(
                pageNumber.toString(),
                style: theme.textTheme.bodyLarge?.apply(
                    color: pageNumber == currentPage
                        ? theme.colorScheme.onPrimary
                        : null),
              ),
            ),
          );
        },
      ),
    );
  }
}
