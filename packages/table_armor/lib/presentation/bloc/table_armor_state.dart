part of 'table_armor_cubit.dart';

enum TableArmorStatus {
  initial,
  loading,
  success,
  failure,
}

class TableArmorState extends Equatable {
  final bool fullVariant;
  final TableArmorStatus status;
  final Object? failureException;
  final int? perPage;
  final ColumnsSizesType columnsSizes;
  final Set<ColumnEntityKeyType> hiddenColumns;
  final List<ColumnEntityKeyType> columnsOrder;
  final TableDataResponse? tableData;
  final DynamicTableSortByType sortBy;
  // Нужно ли принудительно мёрджить фильтра после того как кэш применился. KP-1775
  final bool filterByForceAfterCacheMerge;
  final DynamicTableFilterByInJsonType filterBy;

  const TableArmorState.error(
    Object failureException,
  ) : this(
          failureException: failureException,
          status: TableArmorStatus.failure,
        );

  const TableArmorState({
    this.fullVariant = false,
    this.status = TableArmorStatus.initial,
    this.failureException,
    perPage,
    this.columnsSizes = const {},
    this.hiddenColumns = const {},
    this.columnsOrder = const [],
    this.tableData,
    this.sortBy = const {},
    this.filterByForceAfterCacheMerge = false,
    this.filterBy = const {},
  }) : perPage = perPage ?? 200;

  bool get isLoading => status == TableArmorStatus.loading;

  String get failureMessage => failureException.toString();

  List<ColumnEntity> get columnsToShow =>
      tableData?.columns
          .where((column) => !hiddenColumns.contains(column.key))
          .toList() ??
      [];

  TableArmorState copyWith({
    bool? fullVariant,
    TableArmorStatus? status,
    Exception? failureException,
    int? perPage,
    ColumnsSizesType? columnsSizes,
    Set<ColumnEntityKeyType>? hiddenColumns,
    List<ColumnEntityKeyType>? columnsOrder,
    TableDataResponse? tableData,
    DynamicTableSortByType? sortBy,
    bool? filterByForceAfterCacheMerge,
    DynamicTableFilterByInJsonType? filterBy,
  }) =>
      TableArmorState(
        fullVariant: fullVariant ?? this.fullVariant,
        status: status ?? this.status,
        failureException: failureException ?? this.failureException,
        perPage: perPage ?? this.perPage,
        columnsSizes: columnsSizes ?? this.columnsSizes,
        hiddenColumns: hiddenColumns ?? this.hiddenColumns,
        columnsOrder: columnsOrder ?? this.columnsOrder,
        tableData: tableData ?? this.tableData,
        sortBy: sortBy ?? this.sortBy,
        filterByForceAfterCacheMerge:
            filterByForceAfterCacheMerge ?? this.filterByForceAfterCacheMerge,
        filterBy: filterBy ?? this.filterBy,
      );

  @override
  List<Object?> get props => [
        fullVariant,
        status,
        failureException,
        perPage,
        columnsSizes,
        hiddenColumns,
        columnsOrder,
        tableData,
        sortBy,
        filterByForceAfterCacheMerge,
        filterBy,
      ];
}
