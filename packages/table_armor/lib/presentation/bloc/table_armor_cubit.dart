import 'package:bloc/bloc.dart';
import 'package:cache_data_storage/domain/cache_data_storage.dart';
import 'package:equatable/equatable.dart';
import 'package:injectable/injectable.dart';
import 'package:table/domain/column_sort_type.dart';
import 'package:table/domain/entities/column_entity.dart';
import 'package:table/domain/repositories/dto/table_data_response.dart';
import 'package:table/domain/repositories/dynamic_table_read_repo.dart';
import 'package:table/presentation/column/custom_column_entity_impl.dart';
import 'package:table/presentation/table_widget_controller.dart';

import '../../../application/use_cases/edit_table_use_case.dart';
import '../../../application/use_cases/get_table_data_use_case.dart';
import '../../application/use_cases/get_table_filter_use_case.dart';
import '../../domain/columns_sizes_type.dart';
import '../../domain/exceptions/table_armor_fetch_exception.dart';
import '../../domain/table_id_type.dart';
import '../../infrastructure/dynamic_table_filter_by_in_json_type.dart';
import 'parts/table_armor_cubit_cache_mixin.dart';
import 'parts/table_armor_cubit_checkboxes_column_mixin.dart';
import 'parts/table_armor_cubit_columns_order_mixin.dart';
import 'parts/table_armor_cubit_edit_mixin.dart';
import 'parts/table_armor_cubit_filter_mixin.dart';
import 'parts/table_armor_cubit_hidden_columns_mixin.dart';
import 'parts/table_armor_cubit_resize_mixin.dart';
import 'parts/table_armor_cubit_sorting_mixin.dart';

part 'table_armor_state.dart';

@injectable
class TableArmorCubit extends Cubit<TableArmorState>
    with
        TableArmorCubitSortingMixin,
        TableArmorCubitFilterMixin,
        TableArmorCubitEditMixin,
        TableArmorCubitCacheMixin,
        TableArmorCubitResizeMixin,
        TableArmorCubitCheckboxesColumnMixin,
        TableArmorCubitHiddenColumnsMixin,
        TableArmorCubitColumnsOrderMixin {
  final TableIdType tableId;
  // TODO Ilya: maybe not UseCase, but function?
  final GetTableDataUseCase getTableDataUseCase;
  final GetTableFilterUseCase getTableFilterUseCase;
  final EditTableUseCase editTableUseCase;
  final CacheDataStorage cache;
  final void Function(TableDataResponse)? customModifyTableData;
  final bool hasCheckboxesColumn;
  final List<CustomColumnEntityImpl> customColumns;
  final TableArmorState initState;
  final TableWidgetController controller;
  Future<void>? restoreCacheFuture;

  TableArmorCubit({
    @factoryParam required this.tableId,
    required this.getTableDataUseCase,
    required this.getTableFilterUseCase,
    required this.editTableUseCase,
    required this.cache,
    this.customModifyTableData,
    this.hasCheckboxesColumn = false,
    this.customColumns = const [],
    // TODO Ilya: stateToForceApplyAfterCache
    this.initState = const TableArmorState(
      sortBy: {
        'id': ColumnSortType.DESC,
      },
    ),
    controller,
  })  : controller = controller ?? TableWidgetController(),
        super(initState) {
    // TODO Ilya: or move to the fetchTableData (only on first time)
    restoreCacheFuture = restoreCache();
  }

  // TableState? getTableState() => controller.getTableState();

  void scale(double scale) {
    controller.scale(scale);

    saveCache(CacheFiled.scale, scale);
  }

  void toggleFullVariant() {
    emit(state.copyWith(fullVariant: !state.fullVariant));

    fetchTableData(
      page: state.tableData?.page ?? 1,
    );
    saveCache(CacheFiled.fullVariant, state.fullVariant);
  }

  void changeRowsPerPage(int? perPage) {
    emit(state.copyWith(
      perPage: perPage,
    ));

    fetchTableData(page: 1);
    saveCache(CacheFiled.perPage, perPage);
  }

  Future<void> refresh() => fetchTableData(page: state.tableData?.page ?? 1);

  Future<void> fetchTableData({
    required int page,
  }) async {
    emit(state.copyWith(
      status: TableArmorStatus.loading,
    ));

    try {
      late final TableDataResponse data;

      // Ждём кэш, чтобы было filterBy, sortBy и т.д.
      if (restoreCacheFuture != null) await restoreCacheFuture;

      await Future.wait([
        getTableDataUseCase
            .execute(
              tableId: tableId,
              page: page,
              perPage: state.perPage,
              sortBy: state.sortBy,
              filterBy: state.filterBy,
              fullVariant: state.fullVariant,
            )
            .then((value) => data = value)
            .catchError((e, s) {
          print('$e $s');

          throw TableArmorFetchException(e.toString());
        }),
      ]);

      if (isClosed) return;

      // TODO Ilya: add to the onChange
      restoreColumnsSizes(data.columns);

      for (var column in customColumns) {
        addCustomColumn(
          data: data,
          column: column,
        );
      }

      data.columns = getOrderedColumns(data.columns);

      if (hasCheckboxesColumn) {
        addCheckboxesColumn(data);
      }

      // At the end
      customModifyTableData?.call(data);

      emit(state.copyWith(
        status: TableArmorStatus.success,
        tableData: data,
      ));
    } catch (e, s) {
      print('$e $s');

      if (isClosed) return;

      emit(TableArmorState.error(e));
    }
  }

  @override
  void onChange(Change<TableArmorState> change) {
    super.onChange(change);

    if (change.currentState.hiddenColumns != change.nextState.hiddenColumns ||
        change.currentState.tableData?.rows !=
            change.nextState.tableData?.rows) {
      applyHiddenColumnsToRows(
        change.nextState.tableData?.rows ?? [],
        change.nextState.hiddenColumns,
      );
    }
  }
}
