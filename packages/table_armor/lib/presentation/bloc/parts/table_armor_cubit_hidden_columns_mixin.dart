import 'package:bloc/bloc.dart';
import 'package:injectable/injectable.dart';
import 'package:table/domain/entities/column_entity.dart';
import 'package:table/domain/entities/row_entity.dart';

import '../table_armor_cubit.dart';
import 'table_armor_cubit_cache_mixin.dart';

@injectable
mixin TableArmorCubitHiddenColumnsMixin on Cubit<TableArmorState> {
  TableArmorCubit get bloc => this as TableArmorCubit;

  void setHiddenColumns(Set<ColumnEntityKeyType> columns) {
    emit(state.copyWith(
      hiddenColumns: columns,
    ));

    bloc.saveCache(CacheFiled.hiddenColumns, columns.toList());
  }

  void applyHiddenColumnsToRows(
    List<RowEntity> rows,
    Iterable<ColumnEntityKeyType> columns,
  ) {
    for (var row in rows) {
      row.hideFields(columns.toSet());
    }
  }
}
