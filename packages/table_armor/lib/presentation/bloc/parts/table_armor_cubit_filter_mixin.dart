import 'package:bloc/bloc.dart';
import 'package:injectable/injectable.dart';
import 'package:table/domain/entities/column_entity.dart';
import 'package:table/domain/filters/list_filter.dart';
import 'package:table/domain/repositories/dynamic_table_read_repo.dart';
import 'package:table/infrastructure/filters/table_filters_json_impl.dart';
import 'package:table/infrastructure/mappers/filter_by_map_to_json_mapper.dart';

import '../table_armor_cubit.dart';
import 'table_armor_cubit_cache_mixin.dart';

@injectable
mixin TableArmorCubitFilterMixin on Cubit<TableArmorState> {
  TableArmorCubit get bloc => this as TableArmorCubit;

  Future<FilterItemsType> filterItemsResolver({
    required ColumnEntityKeyType column,
    String? input,
  }) async =>
      (await bloc.getTableFilterUseCase.execute(
        tableId: bloc.tableId,
        column: column,
        input: input,
        fullVariant: bloc.state.fullVariant,
      ))
          .items;

  Future<void> onFilterChange(
    DynamicTableFilterByType<TableFilterJSONImpl> filterBy,
  ) async {
    emit(state.copyWith(
      filterBy: filterBy,
    ));
    _saveToCache(CacheFiled.tableFilterBy, state.filterBy);

    return bloc.fetchTableData(
      page: 1,
    );
  }

  void _saveToCache(
    CacheFiled key,
    DynamicTableFilterByType<TableFilterJSONImpl> filterBy,
  ) {
    final json = FilterByMapToJsonMapper.toJson(
      filterBy,
      isForBackend: false,
    );

    bloc.saveCache(key, json);
  }
}
