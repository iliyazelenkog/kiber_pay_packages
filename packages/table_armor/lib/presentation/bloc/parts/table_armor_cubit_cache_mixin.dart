import 'package:bloc/bloc.dart';
import 'package:injectable/injectable.dart';
import 'package:table/domain/entities/column_entity.dart';
import 'package:table/infrastructure/mappers/filter_by_map_to_json_mapper.dart';
import 'package:table/infrastructure/mappers/sort_map_to_json_mapper.dart';

import '../../../infrastructure/dynamic_table_filter_by_in_json_type.dart';
import '../table_armor_cubit.dart';

enum CacheFiled {
  tableSortBy,
  tableFilterBy,
  fullVariant,
  perPage,
  columnsSizes,
  hiddenColumns,
  columnsOrder,
  // Global (see [cacheFieldsGlobal])
  scale,
}

const cacheFieldsGlobal = <CacheFiled>[
  CacheFiled.scale,
];

@injectable
mixin TableArmorCubitCacheMixin on Cubit<TableArmorState> {
  static const cachePrefixGlobal = 'table_state';

  late final cachePrefix = '${cachePrefixGlobal}_${bloc.tableId}';

  TableArmorCubit get bloc => this as TableArmorCubit;

  Future<void> restoreCache() async {
    try {
      final tableSortByCache = _readCacheField(CacheFiled.tableSortBy);
      final tableFilterByCache = _readCacheField(CacheFiled.tableFilterBy);
      final fullVariantCache = _readCacheField(CacheFiled.fullVariant);
      final perPageCache = _readCacheField(CacheFiled.perPage);
      final columnsSizesCache = _readCacheField(CacheFiled.columnsSizes);
      final hiddenColumnsCache = _readCacheField(CacheFiled.hiddenColumns);
      final columnsOrderCache = _readCacheField(CacheFiled.columnsOrder);

      await Future.wait([
        tableSortByCache,
        tableFilterByCache,
        fullVariantCache,
        perPageCache,
        columnsSizesCache,
        hiddenColumnsCache,
        columnsOrderCache,
        _readCacheField(CacheFiled.scale).then((value) {
          if (value != null) {
            bloc.scale(value);
          }
        }),
      ]);

      late DynamicTableFilterByInJsonType? filterBy;
      if ((await tableFilterByCache) == null) {
        filterBy = null;
      } else {
        filterBy = FilterByMapToJsonMapper.fromJson(
          await tableFilterByCache,
        );
        if (state.filterByForceAfterCacheMerge) {
          filterBy.addAll(state.filterBy);
        }
      }

      // Futures already resolved, so we can safely use await
      emit(state.copyWith(
        fullVariant: await fullVariantCache,
        perPage: await perPageCache,
        columnsSizes: (await columnsSizesCache) == null
            ? null
            : bloc.getColumnSizesFromJson(await columnsSizesCache),
        hiddenColumns: (await hiddenColumnsCache) == null
            ? null
            : (await hiddenColumnsCache as List)
                .cast<ColumnEntityKeyType>()
                .toSet(),
        columnsOrder: (await columnsOrderCache) == null
            ? null
            : (await columnsOrderCache as List)
                .cast<ColumnEntityKeyType>()
                .toList(),
        sortBy: (await tableSortByCache) == null
            ? null
            : SortMapToJsonMapper.fromJson(await tableSortByCache),
        // TODO Ilya: don't call onFilterChange if restored from cache
        filterBy: filterBy,
      ));
    } catch (e, s) {
      print('$e $s');

      clearCache();
    }
  }

  Future<void> clearCache() async {
    await Future.wait(CacheFiled.values.map(
      (field) => bloc.cache.remove(
        getKeyForSave(field),
      ),
    ));
  }

  Future<dynamic> _readCacheField(CacheFiled field) async {
    final cachedData = await bloc.cache.get(
      getKeyForSave(field),
    );

    return cachedData;
  }

  Future<void> saveCache(
    CacheFiled field,
    dynamic value,
  ) async {
    await bloc.cache.save(
      getKeyForSave(
        field,
      ),
      value,
    );
  }

  String getKeyForSave(CacheFiled field) =>
      '${cacheFieldsGlobal.contains(field) ? cachePrefixGlobal : cachePrefix}_${field.name}';
}
