import 'package:bloc/bloc.dart';
import 'package:injectable/injectable.dart';
import 'package:table/domain/entities/column_entity.dart';
import 'package:table/domain/repositories/dto/table_data_response.dart';
import 'package:table/infrastructure/entities/column_entity_impl.dart';
import 'package:table/infrastructure/entities/row_entity_impl.dart';
import 'package:table/presentation/column/custom_column_entity_impl.dart';

import '../table_armor_cubit.dart';

@injectable
mixin TableArmorCubitCheckboxesColumnMixin on Cubit<TableArmorState> {
  static const ColumnEntityKeyType checkboxColumnKey = 'selection_checkboxes';

  TableArmorCubit get bloc => this as TableArmorCubit;

  void addCheckboxesColumn(TableDataResponse data) {
    data.columns.insert(
      0,
      ColumnEntityImpl(
        key: checkboxColumnKey,
        type: ColumnType.checkbox,
        label: 'Выбор',
        editable: false,
        sortable: false,
      ),
    );

    for (var row in data.rows) {
      row.fields = {
        // Add at the beginning
        checkboxColumnKey: RowEntityFieldImpl(
          data: null,
          lightThemeColor: null,
          darkThemeColor: null,
        ),
        ...row.fields,
      }.cast<String, RowEntityFieldImpl>();
    }
  }

  // TODO Ilya: separate logic
  void addCustomColumn({
    required TableDataResponse data,
    required CustomColumnEntityImpl column,
  }) {
    data.columns.insert(
      0,
      column,
    );

    for (var row in data.rows) {
      row.fields = {
        // Add at the beginning
        column.key: RowEntityFieldImpl(
          data: null,
          lightThemeColor: null,
          darkThemeColor: null,
        ),
        ...row.fields,
      }.cast<String, RowEntityFieldImpl>();
    }
  }
}
