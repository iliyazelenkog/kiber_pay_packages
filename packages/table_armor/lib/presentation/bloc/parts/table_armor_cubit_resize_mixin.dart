import 'package:bloc/bloc.dart';
import 'package:collection/collection.dart';
import 'package:injectable/injectable.dart';
import 'package:syncfusion_flutter_datagrid/datagrid.dart';
import 'package:table/domain/entities/column_entity.dart';

import '../../../domain/columns_sizes_type.dart';
import '../table_armor_cubit.dart';
import 'table_armor_cubit_cache_mixin.dart';

@injectable
mixin TableArmorCubitResizeMixin on Cubit<TableArmorState> {
  TableArmorCubit get bloc => this as TableArmorCubit;

  void onColumnResizeUpdate(ColumnResizeUpdateDetails details) {
    // TODO Ilya: debounce
    _saveToCache(details);
  }

  void _saveToCache(ColumnResizeUpdateDetails details) {
    // TODO Ilya: columnName is label (text), not key
    final columnName = details.column.columnName;

    emit(state.copyWith(columnsSizes: {
      ...state.columnsSizes,
      columnName: details.width.toInt(),
    }));

    bloc.saveCache(CacheFiled.columnsSizes, _getColumnSizesJson());
  }

  ColumnsSizesType _getColumnSizesJson() => state.columnsSizes;

  // TODO Ilya: refactor check code logic
  ColumnsSizesType getColumnSizesFromJson(
    Map<String, dynamic> json,
  ) =>
      json.entries.fold(
        {},
        (
          ColumnsSizesType acc,
          entry,
        ) {
          final columnId = entry.key;
          final columnsSize = entry.value;

          acc[columnId] = columnsSize;

          return acc;
        },
      );

  void restoreColumnsSizes(
    List<ColumnEntity> columns,
  ) {
    for (var entry in state.columnsSizes.entries) {
      final cachedColumnKey = entry.key;
      final cachedColumnWidth = entry.value;

      final column = columns.firstWhereOrNull(
        // By label text, not key!
        (e) => e.label == cachedColumnKey,
      );

      column?.resizeWidth(cachedColumnWidth);
    }
  }
}
