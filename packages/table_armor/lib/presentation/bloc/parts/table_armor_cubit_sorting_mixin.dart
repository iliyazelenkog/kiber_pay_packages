import 'package:bloc/bloc.dart';
import 'package:injectable/injectable.dart';
import 'package:table/domain/repositories/dynamic_table_read_repo.dart';
import 'package:table/infrastructure/mappers/sort_map_to_json_mapper.dart';
import 'package:table/presentation/bloc/events/table_event.dart';

import '../table_armor_cubit.dart';
import 'table_armor_cubit_cache_mixin.dart';

@injectable
mixin TableArmorCubitSortingMixin on Cubit<TableArmorState> {
  TableArmorCubit get bloc => this as TableArmorCubit;

  Future<void> changeSort(SortTableEvent event) {
    assert(state.tableData != null);

    final column = event.column;
    final sortType = event.sortType;

    emit(state.copyWith(
      sortBy: {
        column: sortType,
      },
    ));
    _saveToCache(CacheFiled.tableSortBy, state.sortBy);

    return bloc.fetchTableData(
      page: 1,
    );
  }

  void _saveToCache(CacheFiled key, DynamicTableSortByType sortBy) {
    final json = SortMapToJsonMapper.toJson(sortBy);

    bloc.saveCache(key, json);
  }
}
