import 'package:bloc/bloc.dart';
import 'package:injectable/injectable.dart';
import 'package:table/domain/entities/column_entity.dart';

import '../table_armor_cubit.dart';
import 'table_armor_cubit_cache_mixin.dart';

@injectable
mixin TableArmorCubitColumnsOrderMixin on Cubit<TableArmorState> {
  TableArmorCubit get bloc => this as TableArmorCubit;

  void setColumnsOrder(List<ColumnEntityKeyType> columnsOrder) {
    emit(state.copyWith(
      columnsOrder: columnsOrder,
    ));

    bloc.saveCache(CacheFiled.columnsOrder, columnsOrder);
  }

  List<ColumnEntity> getOrderedColumns(List<ColumnEntity> columns) => columns
    ..sort((a, b) {
      final aIndex = state.columnsOrder.indexOf(a.key);
      final bIndex = state.columnsOrder.indexOf(b.key);

      return aIndex.compareTo(bIndex);
    });

  void reorderOnFly() {
    bloc.controller.tableDataSource.setColumns(
      getOrderedColumns(state.tableData?.columns ?? []),
    );
  }
}
