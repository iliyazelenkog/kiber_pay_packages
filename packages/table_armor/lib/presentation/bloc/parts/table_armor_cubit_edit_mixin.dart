import 'package:bloc/bloc.dart';
import 'package:injectable/injectable.dart';
import 'package:table/presentation/bloc/features/edit/table_edit_events.dart';

import '../table_armor_cubit.dart';

@injectable
mixin TableArmorCubitEditMixin on Cubit<TableArmorState> {
  TableArmorCubit get bloc => this as TableArmorCubit;

  Future<void> edit(
    EditTableEvent event,
  ) =>
      bloc.editTableUseCase.execute(
        tableId: bloc.tableId,
        column: event.column.key,
        input: event.newValue,
        rowId: event.row.id,
        fullVariant: bloc.state.fullVariant,
      );
}
