import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:table/presentation/bloc/features/checkboxes/table_bloc_checkboxes_feat.dart';
import 'package:table/presentation/bloc/features/edit/table_edit_events.dart';
import 'package:table/presentation/table_widget.dart';
import 'package:table/presentation/table_widget_controller.dart';
import 'package:table/table.dart';
import 'package:table_armor/presentation/toolbar/table_toolbar.dart';
import 'package:table_armor/presentation/toolbar/table_toolbar_settings_btn.dart';

import '../domain/exceptions/table_armor_fetch_exception.dart';
import '../table_armor_module.dart';
import 'bloc/table_armor_cubit.dart';
import 'mixins/selected_table_rows_mixin.dart';
import 'toolbar/table_toolbar_pagination.dart';
import 'toolbar/table_toolbar_scaling.dart';

class TableView extends StatefulWidget {
  final TableArmorCubit cubit;
  final OnCopyCallback? onCopy;
  final OnEditErrorCallback? onEditError;
  final bool showLoadingIndicator;
  final OnCheckboxesChangedCallback? onCheckboxesChanged;
  final List<Widget>? toolbarChildren;

  const TableView({
    super.key,
    required this.cubit,
    this.onCopy,
    this.onEditError,
    this.showLoadingIndicator = true,
    this.onCheckboxesChanged,
    this.toolbarChildren,
  });

  @override
  State<TableView> createState() => _TableViewState();
}

class _TableViewState extends State<TableView> with SelectedTableRowsMixin {
  TableWidgetController get controller => widget.cubit.controller;

  Future<void> onEdit(EditTableEvent event) async {
    try {
      await widget.cubit.edit(event);
    } catch (e, s) {
      widget.onEditError?.call(context, e, s);
    }
  }

  @override
  Widget build(BuildContext context) => BlocProvider<TableArmorCubit>.value(
        // .value нужен чтобы не вызывался close у кубита, а вызывали его когда сами захотим
        value: widget.cubit,
        child: BlocBuilder<TableArmorCubit, TableArmorState>(
          buildWhen: (p, c) => p.status != c.status,
          builder: (context, state) {
            switch (state.status) {
              case TableArmorStatus.initial:
              case TableArmorStatus.loading:
                // Если таблица уже была загружена, то перебрасываем на success, там свой loader
                if (state.tableData != null) {
                  continue success;
                }

                return const Center(
                  child: CircularProgressIndicator(),
                );
              case TableArmorStatus.failure:
                return Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: [
                    Text('Ошибка. ${state.failureMessage}\n'),
                    if (state.failureException is TableArmorFetchException)
                      ElevatedButton(
                        onPressed: widget.cubit.refresh,
                        child: const Text('Повторить'),
                      ),
                  ],
                );
              success:
              case TableArmorStatus.success:
            }

            final data = state.tableData!;

            // TODO Ilya: подумать как убрать дёргание если мастаб не дефолтный
            // FutureBuilder<TableBloc>(
            //   future: widget.cubit.controller.tableBlocFuture,
            //   builder: (context, tableBlocFutureSnapshot) => Offstage(
            //     // TODO Ilya: better way to handle tableBlocFuture init (problem with init scale if not default)
            //       offstage: tableBlocFutureSnapshot.connectionState !=
            //           ConnectionState.done,
            return Material(
              child: Column(
                children: [
                  TableToolbar(
                    tableCubit: widget.cubit,
                    tableController: controller,
                    selectedRows: selectedRows,
                    onCopy: widget.onCopy,
                    children: [
                      ...?widget.toolbarChildren,
                      // TODO Ilya: remove (Eldar) Maybe move all to the TableToolbar directly?
                      const Spacer(),
                      const TableToolbarScaling(),
                      const TableToolbarPagination(),
                      const TableToolbarSettingsBtn(),
                      const SizedBox(width: 10),
                    ],
                  ),
                  Expanded(
                    child: BlocBuilder<TableArmorCubit, TableArmorState>(
                      buildWhen: (p, c) =>
                          p.isLoading != c.isLoading ||
                          p.columnsToShow != c.columnsToShow,
                      builder: (context, state) => TableWidget(
                        columns: state.columnsToShow,
                        rows: data.rows,
                        filters: state.filterBy,
                        sortBy: state.sortBy,
                        onSort: widget.cubit.changeSort,
                        onEdit: onEdit,
                        onCopy: widget.onCopy,
                        onColumnResizeUpdate: widget.cubit.onColumnResizeUpdate,
                        // TODO Ilya: reset via reseting antifaraud's state (not using controller)
                        onFilterChange: widget.cubit.onFilterChange,
                        isLoading: state.isLoading,
                        filterItemsResolver: widget.cubit.filterItemsResolver,
                        frozenColumnsCount: data.frozenColumnsCount,
                        controller: controller,
                        highlightFooter: data.highlightFooter,
                        showLoadingIndicator: widget.showLoadingIndicator,
                        onCheckboxesChanged: ({
                          rowEntity,
                          required selectedCheckboxes,
                          value,
                        }) {
                          widget.onCheckboxesChanged?.call(
                            rowEntity: rowEntity,
                            selectedCheckboxes: selectedCheckboxes,
                            value: value,
                          );
                          onCheckboxesChanged.call(
                            rowEntity: rowEntity,
                            selectedCheckboxes: selectedCheckboxes,
                            value: value,
                          );
                        },
                      ),
                    ),
                  ),
                ],
              ),
            );
          },
        ),
      );

  // TODO Ilya: I think we don't need this
  // @override
  // void dispose() {
  //   widget.cubit.close();
  //   super.dispose();
  // }
}
