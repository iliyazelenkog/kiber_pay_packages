import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:table_armor/presentation/bloc/table_armor_cubit.dart';

import 'table_toolbar.dart';

class ResetFilter extends StatelessWidget {
  final void Function() onPressResetFilter;

  const ResetFilter({
    super.key,
    required this.onPressResetFilter,
  });
  @override
  Widget build(BuildContext context) {
    final cubit = context.read<TableArmorCubit>();

    return BlocBuilder<TableArmorCubit, TableArmorState>(
      bloc: cubit,
      buildWhen: (p, c) => p.filterBy != c.filterBy,
      builder: (context, state) => state.filterBy.isEmpty
          ? const SizedBox.shrink()
          : Padding(
              padding: const EdgeInsets.only(left: TableToolbar.spaceBetween),
              child: Tooltip(
                message: 'Сбросить фильтры',
                child: IconButton(
                  iconSize: TableToolbar.iconSize,
                  splashRadius: TableToolbar.iconSize,
                  onPressed: onPressResetFilter,
                  icon: const Icon(
                    Icons.filter_alt_off,
                  ),
                ),
              ),
            ),
    );
  }
}
