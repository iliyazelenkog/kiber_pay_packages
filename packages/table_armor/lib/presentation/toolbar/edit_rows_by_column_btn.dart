import 'package:flutter/material.dart';
import 'package:table/domain/entities/column_entity.dart';
import 'package:table/domain/entities/row_entity.dart';
import 'package:table/domain/exceptions/edit_row_column_not_found.dart';
import 'package:table/presentation/table_widget_controller.dart';

class EditRowsByColumnBtn extends StatefulWidget {
  final TableWidgetController tableController;
  final List<RowEntity> rows;
  final String text;
  final VoidCallback? onSuccess;
  final void Function(String error)? onError;
  final List<TableEditBtnWhatToEdit> whatToEdit;

  const EditRowsByColumnBtn({
    super.key,
    required this.tableController,
    required this.rows,
    required this.text,
    required this.whatToEdit,
    this.onSuccess,
    this.onError,
  });

  @override
  State<EditRowsByColumnBtn> createState() => _EditRowsByColumnBtnState();
}

class _EditRowsByColumnBtnState extends State<EditRowsByColumnBtn> {
  late final theme = Theme.of(context);
  var loadingCallbackBtn = false;

  @override
  Widget build(BuildContext context) => ElevatedButton(
        onPressed: loadingCallbackBtn ? null : _edit,
        child: Text(
          widget.text,
        ),
      );

  Future<void> _edit() async {
    setState(() {
      loadingCallbackBtn = true;
    });

    try {
      await Future.wait(
        widget.whatToEdit.map(
          (e) => widget.tableController.editRowsByColumn(
            columnKey: e.columnKey,
            newValue: e.newValue,
            rows: widget.rows,
          ),
        ),
      );

      widget.onSuccess?.call();
    } catch (e) {
      if (e is EditRowColumnNotFound) {
        widget.onError?.call(e.toString());
      }

      widget.onError?.call(
        'Не удалось применить колбэк. $e',
      );

      return;
    }

    setState(() {
      loadingCallbackBtn = false;
    });
  }
}

class TableEditBtnWhatToEdit {
  final ColumnEntityKeyType columnKey;
  final dynamic newValue;

  const TableEditBtnWhatToEdit({
    required this.columnKey,
    required this.newValue,
  });
}
