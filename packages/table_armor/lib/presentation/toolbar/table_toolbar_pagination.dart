import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import '../bloc/table_armor_cubit.dart';
import '../pagination/pagination_widget.dart';
import 'table_toolbar.dart';

class TableToolbarPagination extends StatefulWidget {
  const TableToolbarPagination({
    super.key,
  });

  @override
  State<TableToolbarPagination> createState() => _TableToolbarPaginationState();
}

class _TableToolbarPaginationState extends State<TableToolbarPagination> {
  late final cubit = context.read<TableArmorCubit>();

  @override
  Widget build(BuildContext context) {
    final cubit = context.read<TableArmorCubit>();
    final data = cubit.state.tableData!;

    return Padding(
      padding: const EdgeInsets.only(left: TableToolbar.spaceBetween),
      child: BlocBuilder<TableArmorCubit, TableArmorState>(
        buildWhen: (p, c) => p.perPage != c.perPage,
        builder: (context, state) => PaginationWidget(
          totalPages: data.totalPages,
          page: data.page,
          rowsPerPage: state.perPage,
          pressed: (page) => cubit.fetchTableData(page: page),
          onRowsPerPageSelected: cubit.changeRowsPerPage,
        ),
      ),
    );
  }
}
