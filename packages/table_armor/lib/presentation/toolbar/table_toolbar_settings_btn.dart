import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

import '../bloc/table_armor_cubit.dart';
import 'table_toolbar.dart';
import 'table_toolbar_settings_dialog.dart';

class TableToolbarSettingsBtn extends StatelessWidget {
  const TableToolbarSettingsBtn({
    super.key,
  });

  @override
  Widget build(BuildContext context) => IconButton(
        onPressed: () async {
          final cubit = context.read<TableArmorCubit>();
          final result = await TableToolbarSettingsDialog.show(context);

          if (result != null) {
            cubit
              ..setHiddenColumns(result.hiddenColumns)
              ..setColumnsOrder(result.columnsOrder)
              ..reorderOnFly();
          }
        },
        padding: EdgeInsets.zero,
        icon: const Icon(Icons.settings),
        splashRadius: TableToolbar.iconSize,
      );
}
