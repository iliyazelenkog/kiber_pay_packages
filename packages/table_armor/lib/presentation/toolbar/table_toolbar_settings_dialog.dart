import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:reorderable_grid_view/reorderable_grid_view.dart';
import 'package:table/domain/entities/column_entity.dart';

import '../bloc/parts/table_armor_cubit_checkboxes_column_mixin.dart';
import '../bloc/table_armor_cubit.dart';
import '../dialog/table_dialog.dart';

class TableToolbarSettingsDialogResult {
  final Set<ColumnEntityKeyType> hiddenColumns;
  final List<ColumnEntityKeyType> columnsOrder;

  const TableToolbarSettingsDialogResult({
    required this.hiddenColumns,
    required this.columnsOrder,
  });
}

class TableToolbarSettingsDialog extends StatefulWidget {
  const TableToolbarSettingsDialog({
    super.key,
  });

  static Future<TableToolbarSettingsDialogResult?> show(BuildContext context) =>
      showDialog(
        context: context,
        builder: (_) => Provider<TableArmorCubit>.value(
          value: context.read<TableArmorCubit>(),
          child: const TableToolbarSettingsDialog(),
        ),
      );

  @override
  State<TableToolbarSettingsDialog> createState() =>
      _TableToolbarSettingsDialogState();
}

class _TableToolbarSettingsDialogState
    extends State<TableToolbarSettingsDialog> {
  static const _width = 800.0;
  static const _height = 800.0;

  late final theme = Theme.of(context);
  late final _cubit = context.read<TableArmorCubit>();
  late final Set<ColumnEntityKeyType> _hidden = {..._cubit.state.hiddenColumns};
  late final List<ColumnEntity> _columns =
      _cubit.state.tableData?.columns ?? [];

  List<Widget> get checkboxes => _columns
      .map(
        (e) => IgnorePointer(
          key: ValueKey(e.key),
          // Чекбокс нельзя перемещать
          ignoring:
              e.key == TableArmorCubitCheckboxesColumnMixin.checkboxColumnKey,
          child: CheckboxListTile(
            title: Text(
              '${_columns.indexOf(e) + 1}. ${e.label}',
              style: theme.textTheme.labelMedium,
            ),
            value: !_hidden.contains(e.key),
            enabled: ![
              'id',
              'custom_col_history',
              TableArmorCubitCheckboxesColumnMixin.checkboxColumnKey
            ].contains(e.key),
            onChanged: (value) {
              setState(() {
                value == true ? _hidden.remove(e.key) : _hidden.add(e.key);
              });
            },
            controlAffinity: ListTileControlAffinity.leading,
          ),
        ),
      )
      .toList();

  void _apply() {
    Navigator.of(context).pop(TableToolbarSettingsDialogResult(
      hiddenColumns: _hidden,
      columnsOrder: _columns.map((e) => e.key).toList(),
    ));
  }

  @override
  Widget build(BuildContext context) {
    final theme = Theme.of(context);

    return TableDialog(
      title: 'Настройки',
      actions: [
        SizedBox(
          width: double.infinity,
          height: 40,
          child: ElevatedButton(
            onPressed: _apply,
            child: Text(
              'Применить',
            ),
          ),
        )
      ],
      child: ConstrainedBox(
        constraints: const BoxConstraints(
          maxWidth: _width,
          maxHeight: _height,
        ),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.start,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            const SizedBox(height: 10),
            Padding(
              padding: const EdgeInsets.all(4),
              child: Text(
                'Отображаемые столбцы (можно менять порядок)',
                style: theme.textTheme.titleSmall,
              ),
            ),
            Expanded(
              child: SizedBox(
                width: double.maxFinite,
                child: ReorderableGridView.count(
                  crossAxisCount: 3,
                  childAspectRatio: 5,
                  children: checkboxes,
                  onReorder: (oldIndex, newIndex) {
                    setState(() {
                      final item = _columns.removeAt(oldIndex);
                      _columns.insert(newIndex, item);
                    });
                  },
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
