import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:table_armor/presentation/bloc/table_armor_cubit.dart';

import 'table_toolbar.dart';

class FullVariantSwitch extends StatelessWidget {
  const FullVariantSwitch({
    super.key,
  });

  @override
  Widget build(BuildContext context) {
    final cubit = context.read<TableArmorCubit>();

    return Padding(
      padding: const EdgeInsets.only(left: TableToolbar.spaceBetween),
      child: BlocBuilder<TableArmorCubit, TableArmorState>(
        bloc: cubit,
        buildWhen: (p, c) =>
            p.fullVariant != c.fullVariant || p.isLoading != c.isLoading,
        builder: (context, state) => Row(
          children: [
            SizedBox(
              height: 25,
              child: FittedBox(
                fit: BoxFit.fill,
                child: Switch.adaptive(
                  value: state.fullVariant,
                  onChanged:
                      state.isLoading ? null : (_) => cubit.toggleFullVariant(),
                ),
              ),
            ),
            const SizedBox(width: 5),
            const Text('Полный состав колонок')
          ],
        ),
      ),
    );
  }
}
