import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:table_armor/presentation/bloc/table_armor_cubit.dart';

import 'table_toolbar.dart';

class TableToolbarRefreshBtn extends StatelessWidget {
  final VoidCallback? onPressed;

  const TableToolbarRefreshBtn({
    super.key,
    this.onPressed,
  });

  @override
  Widget build(BuildContext context) {
    final tableCubit = context.read<TableArmorCubit>();

    return Tooltip(
      message: 'Обновить таблицу',
      child: BlocBuilder<TableArmorCubit, TableArmorState>(
        bloc: tableCubit,
        buildWhen: (p, c) => p.isLoading != c.isLoading,
        builder: (context, state) => state.isLoading
            ? const Padding(
                padding: EdgeInsets.only(left: 10, right: 15),
                child: SizedBox.square(
                  dimension: TableToolbar.iconSize - 5,
                  child: CircularProgressIndicator(
                    strokeWidth: 2.5,
                  ),
                ),
              )
            : IconButton(
                iconSize: TableToolbar.iconSize,
                splashRadius: TableToolbar.iconSize,
                onPressed: tableCubit.refresh,
                icon: const Icon(Icons.refresh),
              ),
      ),
    );
  }
}
