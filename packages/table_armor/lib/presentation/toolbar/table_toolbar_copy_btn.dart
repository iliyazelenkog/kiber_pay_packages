import 'package:flutter/material.dart';

import 'table_toolbar.dart';

class TableToolbarCopyBtn extends StatelessWidget {
  final VoidCallback? onPressed;

  const TableToolbarCopyBtn({
    super.key,
    this.onPressed,
  });

  @override
  Widget build(BuildContext context) => Padding(
        padding: const EdgeInsets.only(left: TableToolbar.spaceBetween),
        child: Tooltip(
          message: 'Скопировать выбранные',
          child: IconButton(
            iconSize: TableToolbar.iconSize,
            splashRadius: TableToolbar.iconSize,
            onPressed: onPressed,
            icon: const Icon(Icons.copy),
          ),
        ),
      );
}
