import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:table/domain/entities/row_entity.dart';
import 'package:table/presentation/table_widget_controller.dart';
import 'package:table/table.dart';
import 'package:table_armor/infrastructure/dynamic_table_filter_by_in_json_type.dart';
import 'package:table_armor/presentation/bloc/table_armor_cubit.dart';

import 'full_variant_switch.dart';
import 'reset_filter.dart';
import 'table_toolbar_copy_btn.dart';
import 'table_toolbar_refresh_btn.dart';

abstract class ToolbarBtnWithAfter {
  Type? get after;
}

class TableToolbar extends StatelessWidget {
  final TableArmorCubit tableCubit;
  final TableWidgetController tableController;
  final List<RowEntity> selectedRows;
  final List<Widget>? children;
  // Стартовая фильтрация. Если делается сбро фильтров, то к этому значению
  final DynamicTableFilterByInJsonType? initFilterBy;
  final OnCopyCallback? onCopy;

  const TableToolbar({
    super.key,
    required this.tableCubit,
    required this.tableController,
    this.selectedRows = const [],
    this.onCopy,
    this.initFilterBy,
    this.children,
  });

  static const iconSize = 20.0;
  static const spaceBetween = 15.0;

  List<Widget> getItems(BuildContext context) {
    var items = <Widget>[
      const SizedBox(width: 10),
      const TableToolbarRefreshBtn(),
      const FullVariantSwitch(),
      ResetFilter(
        onPressResetFilter: () =>
            tableController.setFilterItems(initFilterBy ?? {}),
      ),
      if (selectedRows.isNotEmpty)
        TableToolbarCopyBtn(
          onPressed: () => copyRows(context),
        ),
    ];

    if (children != null) {
      for (var e in children!) {
        if (e is ToolbarBtnWithAfter) {
          final after = (e as ToolbarBtnWithAfter).after;

          final index = items.indexWhere((btn) => btn.runtimeType == after);

          if (index != -1) {
            items.insert(index + 1, e);
          }
        } else {
          items.add(e);
        }
      }
    }

    return items;
  }

  void copyRows(BuildContext context) {
    final text = tableController.copyRows(selectedRows);
    onCopy?.call(context, text);
  }

  @override
  Widget build(BuildContext context) => Provider<TableArmorCubit>(
        create: (_) => tableCubit,
        child: Row(
          children: getItems(context),
        ),
      );
}
