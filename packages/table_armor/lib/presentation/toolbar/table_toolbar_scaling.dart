import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:table/presentation/bloc/table_bloc.dart';

import '../bloc/table_armor_cubit.dart';
import 'table_toolbar.dart';

class TableToolbarScaling extends StatefulWidget {
  const TableToolbarScaling({
    super.key,
  });

  @override
  State<TableToolbarScaling> createState() => _TableToolbarScalingState();
}

class _TableToolbarScalingState extends State<TableToolbarScaling> {
  late final cubit = context.read<TableArmorCubit>();

  static const _minScale = 0.5;
  static const _maxScale = 1.5;

  void _scaleWithClamp(
    double value,
  ) async {
    final newScale = value.clamp(
      _minScale,
      _maxScale,
    );

    cubit.scale(newScale);
  }

  @override
  Widget build(BuildContext context) => Padding(
        padding: const EdgeInsets.only(left: TableToolbar.spaceBetween),
        child: FutureBuilder<TableBloc>(
          future: cubit.controller.tableBlocFuture,
          builder: (context, snapshot) {
            if (snapshot.connectionState != ConnectionState.done) {
              return const SizedBox.square(
                dimension: TableToolbar.iconSize,
                child: CircularProgressIndicator(),
              );
            }

            final tableBloc = snapshot.data;

            return BlocBuilder<TableBloc, TableState>(
              bloc: tableBloc!,
              buildWhen: (p, c) => p.scale != c.scale,
              builder: (context, state) {
                return Row(
                  children: [
                    IconButton(
                      onPressed: state.scale == _minScale
                          ? null
                          : () async {
                              _scaleWithClamp(
                                state.scale + -0.1,
                              );
                            },
                      padding: EdgeInsets.zero,
                      icon: const Icon(Icons.zoom_out),
                      splashRadius: TableToolbar.iconSize,
                    ),
                    Text(
                      '${(state.scale * 100).round()}%',
                      style: Theme.of(context).textTheme.bodyMedium,
                    ),
                    IconButton(
                      onPressed: state.scale == _maxScale
                          ? null
                          : () async {
                              _scaleWithClamp(
                                state.scale + 0.1,
                              );
                            },
                      padding: EdgeInsets.zero,
                      icon: const Icon(Icons.zoom_in),
                      splashRadius: TableToolbar.iconSize,
                    ),
                  ],
                );
              },
            );
          },
        ),
      );
}
