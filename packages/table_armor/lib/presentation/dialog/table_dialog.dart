import 'package:flutter/material.dart';

import 'table_dialog_header.dart';

class TableDialog extends StatelessWidget {
  final String title;
  final Widget? subTitle;
  final Widget child;
  final List<Widget>? actions;
  final double topPadding;
  final EdgeInsets contentPadding;

  TableDialog({
    super.key,
    required this.title,
    this.subTitle,
    required this.child,
    this.actions,
    this.topPadding = 0,
    contentPadding,
  }) : contentPadding = contentPadding ??
            EdgeInsets.fromLTRB(
              16,
              topPadding,
              16,
              0,
            );

  @override
  Widget build(BuildContext context) {
    final theme = Theme.of(context);

    return AlertDialog(
      contentPadding: contentPadding,
      titlePadding: EdgeInsets.zero,
      backgroundColor: theme.canvasColor,
      title: subTitle == null
          ? TableDialogHeader(
              title: title,
            )
          : Column(
              children: [
                TableDialogHeader(
                  title: title,
                ),
                subTitle!
              ],
            ),
      content: child,
      actions: actions,
    );
  }
}
