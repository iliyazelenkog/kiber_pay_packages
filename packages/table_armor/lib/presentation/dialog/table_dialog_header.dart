import 'package:flutter/material.dart';

class TableDialogHeader extends StatelessWidget {
  final String title;
  final Function()? onPressed;

  const TableDialogHeader({
    super.key,
    required this.title,
    this.onPressed,
  });

  @override
  Widget build(BuildContext context) {
    return Container(
      height: 56,
      decoration: BoxDecoration(
        border: Border(
          bottom: BorderSide(
            width: 1,
            color: Theme.of(context).scaffoldBackgroundColor,
          ),
        ),
      ),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          Padding(
            padding: const EdgeInsets.only(left: 16),
            child: Text(title),
          ),
          _CloseButtonWidget(onPressed: () {
            if (onPressed != null) {
              onPressed!();
            }
            Navigator.of(context).pop();
          })
        ],
      ),
    );
  }
}

class _CloseButtonWidget extends StatelessWidget {
  final Function() onPressed;

  const _CloseButtonWidget({
    super.key,
    required this.onPressed,
  });

  @override
  Widget build(BuildContext context) => IconButton(
        onPressed: onPressed,
        color: Theme.of(context).iconTheme.color,
        padding: const EdgeInsets.all(16),
        icon: const Icon(Icons.close),
        splashRadius: 24,
      );
}
