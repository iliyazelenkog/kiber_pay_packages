import 'package:flutter/widgets.dart';
import 'package:table/domain/entities/row_entity.dart';

// TODO Ilya: create TableSelectableRows widget to control selectedRows insteadof SelectedTableRowsMixin?
// and selectedRows will be provided using Provider
mixin SelectedTableRowsMixin<T extends StatefulWidget> on State<T> {
  var selectedRows = <RowEntity>[];

  void onCheckboxesChanged({
    RowEntity? rowEntity,
    bool? value,
    required List<RowEntity> selectedCheckboxes,
  }) {
    setState(() {
      selectedRows = selectedCheckboxes;
    });
  }
}
