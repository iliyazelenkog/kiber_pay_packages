import 'dart:async';

import 'package:modules_basis/application/use_case.dart';
import 'package:table/domain/repositories/dto/table_data_response.dart';
import 'package:table/domain/repositories/dynamic_table_read_repo.dart';
import 'package:table_armor/domain/table_id_type.dart';

abstract class GetTableDataUseCase implements UseCase {
  const GetTableDataUseCase();

  Future<TableDataResponse> execute({
    required TableIdType tableId,
    required int page,
    int? perPage,
    DynamicTableSortByType? sortBy,
    DynamicTableFilterByType? filterBy,
    fullVariant = false,
  });
}
