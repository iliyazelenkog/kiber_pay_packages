import 'dart:async';

import 'package:modules_basis/application/use_case.dart';
import 'package:table/domain/entities/column_entity.dart';
import 'package:table/domain/entities/row_entity.dart';
import 'package:table_armor/domain/table_id_type.dart';

abstract class EditTableUseCase implements UseCase {
  const EditTableUseCase();

  Future<void> execute({
    required TableIdType tableId,
    required ColumnEntityKeyType column,
    required dynamic input,
    required RowIdType rowId,
    fullVariant = false,
  });
}
