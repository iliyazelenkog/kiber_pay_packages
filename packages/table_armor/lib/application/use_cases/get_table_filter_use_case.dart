import 'dart:async';

import 'package:modules_basis/application/use_case.dart';
import 'package:table/domain/entities/column_entity.dart';
import 'package:table/domain/repositories/dto/table_filter_response.dart';

import '../../domain/table_id_type.dart';

abstract class GetTableFilterUseCase implements UseCase {
  const GetTableFilterUseCase();

  Future<TableFilterResponse> execute({
    required TableIdType tableId,
    required ColumnEntityKeyType column,
    required String? input,
    fullVariant = false,
  });
}
