import 'package:table/domain/repositories/dynamic_table_read_repo.dart';
import 'package:table/infrastructure/filters/table_filters_json_impl.dart';

// Фильтры для каждой колонки в формате JSON
typedef DynamicTableFilterByInJsonType
    = DynamicTableFilterByType<TableFilterJSONImpl>;
