import 'dart:async';

import 'package:flutter/cupertino.dart';
import 'package:injectable/injectable.dart';
import 'package:modules_basis/module.dart' as module;
import 'package:table/table.module.dart';

typedef OnEditErrorCallback = void Function(
  BuildContext context,
  Object e,
  StackTrace s,
);

@InjectableInit.microPackage(
  externalPackageModulesBefore: [
    ExternalModule(TablePackageModule),
    // ExternalModule(CacheDataStoragePackageModule),
  ],
)
@lazySingleton
class TableArmorModule extends module.Module<TableArmorModuleResult> {
  @override
  Future<TableArmorModuleResult> execute() async {
    return TableArmorModuleResult();
  }
}

class TableArmorModuleResult extends module.ModuleResult {}
